#![no_main]
use libfuzzer_sys::fuzz_target;
use core_alloc::slicetree::fuzzing::OwnedSliceTree;

// We need some kind of arbitrary-safe data.
fuzz_target!(|data: OwnedSliceTree<u8>| {
    // Check the length
    assert_eq!(data.slice_tree_ref().len(), data.flat_slice().len());

    // Check that every index is the same
    for (raw_idx, item) in data.slice_tree_ref().into_iter().enumerate() {
        assert_eq!(data.flat_slice().get(raw_idx), Some(item));
        // Check the raw index getting
        assert_eq!(data.flat_slice().get(raw_idx), data.slice_tree_ref().get(raw_idx));
    }
});





// core-alloc
// Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
