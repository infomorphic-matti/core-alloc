#![no_main]
use chain_trans::prelude::*;
use core::{alloc::Layout, ptr::NonNull};
use core_alloc::{
    fuzzing::AllocatorEventSequence,
    sptr::{self, Strict},
    AllocError, ExtendedAllocator,
};
use libfuzzer_sys::fuzz_target;

/// Shims in an ultra-barebones global allocator delegation.
struct FakeAllocator;

/// Gets the innards of Layout::dangling nightly.
///
/// Do NOT attempt to access the pointer contents in any way.
fn dangling(l: Layout) -> NonNull<u8> {
    sptr::invalid_mut::<u8>(l.align())
        .trans(NonNull::new)
        .expect("Layout::dangling assumes nonzero so we do too")
}

unsafe impl ExtendedAllocator for FakeAllocator {

    // Problems - zeroing causes issues here because of the use of a fake allocation where the
    // other methods read and write.
    //
    // Solution - grabbing the checks from ExtendedAllocator default methods and replacing them
    // with just the checks
    fn allocate(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
        let data_base = dangling(layout);
        let data_len = layout.size();
        let new_fake_slice = core::ptr::slice_from_raw_parts_mut(data_base.as_ptr(), data_len)
            .trans(NonNull::new)
            .expect("Just got out of a nonnull");

        Ok(new_fake_slice)
    }


    fn allocate_zeroed(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
        self.allocate(layout)
    }
    
    unsafe fn grow(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        debug_assert!(
            new_layout.size() >= old_layout.size(),
            "`new_layout.size()` must be greater than or equal to `old_layout.size()`"
        );

        let new_ptr = self.allocate(new_layout)?;

        // SAFETY: because `new_layout.size()` must be greater than or equal to
        // `old_layout.size()`, both the old and new memory allocation are valid for reads and
        // writes for `old_layout.size()` bytes. Also, because the old allocation wasn't yet
        // deallocated, it cannot overlap `new_ptr`. Thus, the call to `copy_nonoverlapping` is
        // safe. The safety contract for `dealloc` must be upheld by the caller.
        unsafe {
            /*
            ptr::copy_nonoverlapping(
                ptr.as_ptr(),
                as_non_null_ptr(new_ptr).as_ptr(),
                old_layout.size(),
            );
            */
            self.deallocate(ptr, old_layout);
        }

        Ok(new_ptr)
    }
    
    unsafe fn grow_zeroed(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        self.grow(ptr, old_layout, new_layout)
    }


    
    
    unsafe fn shrink(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        debug_assert!(
            new_layout.size() <= old_layout.size(),
            "`new_layout.size()` must be smaller than or equal to `old_layout.size()`"
        );

        let new_ptr = self.allocate(new_layout)?;

        // SAFETY: because `new_layout.size()` must be lower than or equal to
        // `old_layout.size()`, both the old and new memory allocation are valid for reads and
        // writes for `new_layout.size()` bytes. Also, because the old allocation wasn't yet
        // deallocated, it cannot overlap `new_ptr`. Thus, the call to `copy_nonoverlapping` is
        // safe. The safety contract for `dealloc` must be upheld by the caller.
        unsafe {
            /*
            ptr::copy_nonoverlapping(
                ptr.as_ptr(),
                as_non_null_ptr(new_ptr).as_ptr(),
                new_layout.size(),
            );
            */
            self.deallocate(ptr, old_layout);
        }

        Ok(new_ptr)
    }
    
    unsafe fn split(
        &self,
        old_allocation: NonNull<u8>,
        old_layout: Layout,
        new_layout_start: Layout,
        new_layout_end: Layout,
    ) -> Result<(NonNull<[u8]>, NonNull<[u8]>), AllocError> {
        debug_assert!(
            new_layout_start.size() + new_layout_end.size() <= old_layout.size(),
            "`new_layout_start.size() + new_layout_end.size()` must be less than or equal to `old_layout.size()`"
        );
        // Default implementations allocate for the second half of the layout, copy from the old
        // layout, and shrink the old allocation to match the front layout.
        //
        // This means that the only part of the allocation that can damage the input is last, so an
        // intermediary process succeeding does not wreck the original memory location in case a
        // latter part fails, preserving atomicity.
        let new_second_half = self.allocate(new_layout_end)?;

        // Copy memory - This is safe, because the two new layouts must be <= the old layout
        // length, so they are always good to read.
        //
        // The old memory and new memory can't overlap either because the old memory has not been
        // shrunk or otherwise deallocated yet.

        /*
        unsafe {
            // get the second chunk and copy over :)
            ptr::copy_nonoverlapping(
                old_allocation.as_ptr().add(new_layout_start.size()),
                as_non_null_ptr(new_second_half).as_ptr(),
                new_layout_end.size(),
            );
        }
        */

        // Shrink the original
        // SAFETY:
        // * new_layout_start.size() + new_layout_end.size() <= old_layout.size()
        //   => new_layout_start.size() <= old_layout.size()
        // * Old layout must fit in the original block, as defined by the function contract
        // * old_allocation must denote a block produced by this allocator, as defined in the
        // function safety contract.
        unsafe { self.shrink(old_allocation, old_layout, new_layout_start) }
            .map(|new_first_half| (new_first_half, new_second_half))
    }


    // Pointers are FAKE ANYWAY :)
    unsafe fn deallocate(&self, _ptr: NonNull<u8>, _layout: Layout) {}
}

fuzz_target!(
    |data: core_alloc::fuzzing::AllocatorEventSequence<10_000>| {
        data.basic_extended_allocator_run(&FakeAllocator)
    }
);
