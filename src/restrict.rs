//! When processing input and output, it is often desireable to control and limit memory usage -
//! for instance, to protect from malicious inputs that attempt to prevent your software from
//! working, or just because you are in a highly constrained environment.
//!
//! This module contains allocators that can wrap other allocators and restrict memory usage in one
//! of two ways. In particular, there are *soft* and *hard* restrictions on memory usage.
//!
//! # Rust Allocators
//!
//! Rust allocators require their fallible operations (such as `grow` and `shrink`) to be
//! atomic - that is, they either fail but leave the original memory in a usable state, or
//! they succeed and produce new chunks of valid memory.
//!
//! This is important to restricting memory usage without allocator cooperation, because it means
//! that without allocator cooperation, it is unsound to terminate the operation of an inner
//! allocator after it has successfully produced new memory chunks, because then the original
//! memory is invalidated. This also applies to the alloc function, because terminating after new
//! memory is allocated means that it leaks.
//!

use core::alloc::Layout;

#[inline]
/// This is also unstable... and taken from standard lib [`Layout::padding_needed_for`].
///
/// Gets the padding needed for the layout to fill a collection of blocks aligned to the given
/// alignment.
const fn padding_needed_for(layout: Layout, align: usize) -> usize {
    let len = layout.size();

    // Rounded up value is:
    //   len_rounded_up = (len + align - 1) & !(align - 1);
    // and then we return the padding difference: `len_rounded_up - len`.
    //
    // We use modular arithmetic throughout:
    //
    // 1. align is guaranteed to be > 0, so align - 1 is always
    //    valid.
    //
    // 2. `len + align - 1` can overflow by at most `align - 1`,
    //    so the &-mask with `!(align - 1)` will ensure that in the
    //    case of overflow, `len_rounded_up` will itself be 0.
    //    Thus the returned padding, when added to `len`, yields 0,
    //    which trivially satisfies the alignment `align`.
    //
    // (Of course, attempts to allocate blocks of memory whose
    // size and padding overflow in the above manner should cause
    // the allocator to yield an error anyway.)

    let len_rounded_up = len.wrapping_add(align).wrapping_sub(1) & !align.wrapping_sub(1);
    len_rounded_up.wrapping_sub(len)
}

/// Get the minimum size of the block that can be allocated for the given layout
/// In particular, this requires that the size be increased to the least multiple of it's alignment
///
/// Note that this is basically [`Layout::pad_to_align`], which isn't const for no reason I can
/// discern
const fn aligned_minimum_block_size(layout: Layout) -> usize {
    let pad = padding_needed_for(layout, layout.align());
    // This cannot overflow. Quoting from the invariant of Layout:
    // > `size`, when rounded up to the nearest multiple of `align`,
    // > must not overflow isize (i.e., the rounded value must be
    // > less than or equal to `isize::MAX`)
    layout.size() + pad
}

/// Contains implementation for soft memory usage restriction
///
/// # Soft Restrictions
/// Soft restrictions on memory usage require minimal cooperation from the allocator, but provide
/// weaker constraints on how much real memory is actually used. This is because they can't
/// terminate an operation that the original allocator already completed successfully.
///
/// This means they can only do "best effort" restriction. In particular, it means that they can
/// only really count based on the size and alignment. If the allocator is attacked by forcing the
/// program to allocate lots of small amounts of memory such that excess is constantly allocated by
/// the allocator (as most allocators allocate in larger chunks), it can still lead to memory
/// exhaustion in the pathological case.
///
/// Unfortunately it can't even update an internal count with the lengths of the returned memory,
/// because that information is not carried through to the deallocator (where it would need to be
/// subtracted).
///
/// Furthermore, rust allocators require that, upon deallocation, the layout only *fits* the memory
/// block. this means in practise that the layout can be anywhere from the original size to a
/// larger size as long as it fits in whatever memory block was actually returned.
///
/// For soft restriction, this means that deallocation is over-optimistic in how it estimates the
/// amount of used memory, because a larger layout may be passed to the deallocation than the
/// allocation.
pub mod soft {
    use core::cell::Cell;

    use crate::{alloc_error, AllocError, ExtendedAllocator};

    use super::aligned_minimum_block_size;

    /// The old layout to-deallocate aligned blocksize is how much an allocation "currently" is.
    /// It may be larger than the original allocation as specified by memory "fitting".
    /// If only allocating, this should be zero
    ///
    /// The new layout aligned blocksize is the heuristic for the total minimum # of bytes
    /// needed for the new layout(s). If deallocating, this should be zero - and if it is zero this
    /// is guarunteed not to return an error. Note that this allows for shrinking of any kind not
    /// just deallocation
    ///
    /// Returns the new internal allocated-bytes-count heuristic if there was no overflow and the
    /// new amount of estimated bytes wasn't too big, returns an allocation error otherwise. You
    /// should not update the internal count until the actual allocation/deallocation is
    /// successfully completed.
    #[inline]
    const fn allocation_delta(
        current_allocated_heuristic: usize,
        to_deallocate_blocksize_heuristic: usize,
        to_allocate_blocksize_heuristic: usize,
        max_bytes: usize,
    ) -> Result<usize, AllocError> {
        let with_old_layout_dealloc =
            current_allocated_heuristic.saturating_sub(to_deallocate_blocksize_heuristic);
        // No const ok_or on option
        let with_new_layout_heuristic =
            match with_old_layout_dealloc.checked_add(to_allocate_blocksize_heuristic) {
                Some(v) => v,
                None => return Err(alloc_error()),
            };
        // In the case that we deallocate more than we allocate, do not error.
        //
        // This is to account for if the maximum bytes have changed since a previous alloc
        if with_new_layout_heuristic > max_bytes
            && to_allocate_blocksize_heuristic >= to_deallocate_blocksize_heuristic
        {
            Err(alloc_error())
        } else {
            Ok(with_new_layout_heuristic)
        }
    }

    #[derive(Debug)]
    /// [Soft-Restricted](#soft-restrictions) allocator that is restricted to a
    /// statically-determined number of bytes, that passes through to the inner allocator.
    ///
    /// The limit is `heuristic <= max bytes` - so it's inclusive.
    ///
    /// Note that this is not safe to use between threads, and is not cloneable. This is because if
    /// it were cloneable the byte counts would essentially bifurcate between clones. We only
    /// implement allocator on references to this anyway.
    ///
    /// Allocations beyond the limit of usize always result in errors.
    ///
    /// # Notes on Growing, Shrinking & Splitting
    /// Without cooperation from the allocator, it is impossible for a soft memory usage
    /// restricted allocator to understand whether the inner allocator uses inner memory
    /// efficiently inside methods related to growing and shrinking and splitting.
    ///
    /// In particular,
    /// it can't bound any *extra* allocations that may occur inside a growing, shrinking or
    /// splitting implementation as a temporary measure to perform copying, as occurs in default
    /// implementations.
    ///
    /// It uses minimal memory usage estimates - that is, it calculates the minimum memory block
    /// size for both inputs and outputs and combines them apropriately to estimate how much memory
    /// was actually used.
    pub struct Fixed<const MAX_BYTES: usize, A: ExtendedAllocator> {
        heuristic_byte_count: Cell<usize>,
        inner_allocator: A,
    }

    impl<const MAX_BYTES: usize, A: ExtendedAllocator> Fixed<MAX_BYTES, A> {
        /// Count bytes allocated on the passed allocator that are allocated *via* this one,
        /// restricted to a statically determined number.
        #[inline]
        pub fn new(allocator: A) -> Self {
            Self {
                heuristic_byte_count: Cell::default(),
                inner_allocator: allocator,
            }
        }

        /// Try and perform an allocation operation if there's enough available space - erroring
        /// either before attempting allocation, or if it succeeded, updating the contents of the
        /// counter.
        ///
        /// Guarunteed not to fail if the inner operation doesn't fail and we solely deallocate.
        #[inline]
        fn allocator_operation<Q>(
            &self,
            old_layout_aligned_blocksize: usize,
            new_layouts_total_aligned_blocksize: usize,
            operation: impl FnOnce(&A) -> Result<Q, AllocError>,
        ) -> Result<Q, AllocError> {
            let current_count = self.heuristic_byte_count.get();
            let new_count = allocation_delta(
                current_count,
                old_layout_aligned_blocksize,
                new_layouts_total_aligned_blocksize,
                MAX_BYTES,
            )?;
            let output = operation(&self.inner_allocator)?;
            self.heuristic_byte_count.set(new_count);
            Ok(output)
        }
    }

    /// SAFETY - All of these forward to inner functions. which satisfy the function requirements,
    /// and similar.
    #[allow(unsafe_op_in_unsafe_fn)]
    unsafe impl<const MAX_BYTES: usize, A: ExtendedAllocator> ExtendedAllocator
        for &'_ Fixed<MAX_BYTES, A>
    {
        #[inline]
        fn allocate(
            &self,
            layout: core::alloc::Layout,
        ) -> Result<core::ptr::NonNull<[u8]>, AllocError> {
            // Use the provided alignment as a heuristic
            let min_block_size = aligned_minimum_block_size(layout);
            self.allocator_operation(0, min_block_size, |a| a.allocate(layout))
        }

        #[inline]
        unsafe fn deallocate(&self, ptr: core::ptr::NonNull<u8>, layout: core::alloc::Layout) {
            // Fitting means this is only approximate, but it is unfortunately always an
            // overestimate :/
            let min_allocated_block_size = aligned_minimum_block_size(layout);
            self.allocator_operation(min_allocated_block_size, 0, |a| {
                a.deallocate(ptr, layout);
                Ok(())
            })
            .expect("Allocation operation guarunteed to succeed with only deallocation occurring")
        }

        #[inline]
        fn allocate_zeroed(
            &self,
            layout: core::alloc::Layout,
        ) -> Result<core::ptr::NonNull<[u8]>, AllocError> {
            // Use the provided alignment as a heuristic
            let min_block_size = aligned_minimum_block_size(layout);
            self.allocator_operation(0, min_block_size, |a| a.allocate_zeroed(layout))
        }

        #[inline]
        unsafe fn grow(
            &self,
            ptr: core::ptr::NonNull<u8>,
            old_layout: core::alloc::Layout,
            new_layout: core::alloc::Layout,
        ) -> Result<core::ptr::NonNull<[u8]>, AllocError> {
            debug_assert!(
                new_layout.size() >= old_layout.size(),
                "`new_layout.size()` must be greater than or equal to `old_layout.size()`"
            );

            let min_dealloc_block_size = aligned_minimum_block_size(old_layout);
            let min_realloc_block_size = aligned_minimum_block_size(new_layout);
            self.allocator_operation(min_dealloc_block_size, min_realloc_block_size, |a| {
                a.grow(ptr, old_layout, new_layout)
            })
        }

        #[inline]
        unsafe fn grow_zeroed(
            &self,
            ptr: core::ptr::NonNull<u8>,
            old_layout: core::alloc::Layout,
            new_layout: core::alloc::Layout,
        ) -> Result<core::ptr::NonNull<[u8]>, AllocError> {
            debug_assert!(
                new_layout.size() >= old_layout.size(),
                "`new_layout.size()` must be greater than or equal to `old_layout.size()`"
            );

            let min_dealloc_block_size = aligned_minimum_block_size(old_layout);
            let min_realloc_block_size = aligned_minimum_block_size(new_layout);
            self.allocator_operation(min_dealloc_block_size, min_realloc_block_size, |a| {
                a.grow_zeroed(ptr, old_layout, new_layout)
            })
        }

        #[inline]
        unsafe fn shrink(
            &self,
            ptr: core::ptr::NonNull<u8>,
            old_layout: core::alloc::Layout,
            new_layout: core::alloc::Layout,
        ) -> Result<core::ptr::NonNull<[u8]>, AllocError> {
            debug_assert!(
                new_layout.size() <= old_layout.size(),
                "`new_layout.size()` must be smaller than or equal to `old_layout.size()`"
            );

            let min_dealloc_block_size = aligned_minimum_block_size(old_layout);
            let min_realloc_block_size = aligned_minimum_block_size(new_layout);
            self.allocator_operation(min_dealloc_block_size, min_realloc_block_size, |a| {
                a.shrink(ptr, old_layout, new_layout)
            })
        }

        #[inline(always)]
        fn by_ref(&self) -> &Self
        where
            Self: Sized,
        {
            self
        }

        #[inline]
        unsafe fn split(
            &self,
            old_allocation: core::ptr::NonNull<u8>,
            old_layout: core::alloc::Layout,
            new_layout_start: core::alloc::Layout,
            new_layout_end: core::alloc::Layout,
        ) -> Result<(core::ptr::NonNull<[u8]>, core::ptr::NonNull<[u8]>), AllocError> {
            debug_assert!(
                new_layout_start.size() + new_layout_end.size() <= old_layout.size(),
                "`new_layout_start.size() + new_layout_end.size()` must be less than or equal to `old_layout.size()`"
            );

            let dealloc_heuristic = aligned_minimum_block_size(old_layout);
            let realloc_heuristic = aligned_minimum_block_size(new_layout_start)
                + aligned_minimum_block_size(new_layout_end);
            self.allocator_operation(dealloc_heuristic, realloc_heuristic, |a| {
                a.split(old_allocation, old_layout, new_layout_start, new_layout_end)
            })
        }
    }

    /// [Soft-Restricted](#soft-restrictions) allocator that is restricted to a
    /// dynamically-determined number of bytes, that passes through to the inner allocator.
    ///
    /// This has essentially the same behaviour as [`Fixed`], but it uses a runtime determined -
    /// and runtime-adjustable - memory limit.
    ///
    /// In particular, decreasing the runtime memory limit will not cause pure deallocations to
    /// error, and will also allow shrinking.
    pub struct Dynamic<A: ExtendedAllocator> {
        maximum: Cell<usize>,
        heuristic_byte_count: Cell<usize>,
        inner_allocator: A,
    }

    impl<A: ExtendedAllocator> Dynamic<A> {
        /// Count bytes allocated on the passed allocator that are allocated *via* this one,
        /// restricted to a dynamically determined number.
        #[inline]
        pub fn new(allocator: A, initial_soft_limit: usize) -> Self {
            Self {
                heuristic_byte_count: Cell::default(),
                inner_allocator: allocator,
                maximum: initial_soft_limit.into(),
            }
        }

        /// Try and perform an allocation operation if there's enough available space - erroring
        /// either before attempting allocation, or if it succeeded, updating the contents of the
        /// counter.
        ///
        /// Guarunteed not to fail if the inner operation doesn't fail and we solely deallocate.
        #[inline]
        fn allocator_operation<Q>(
            &self,
            old_layout_aligned_blocksize: usize,
            new_layouts_total_aligned_blocksize: usize,
            operation: impl FnOnce(&A) -> Result<Q, AllocError>,
        ) -> Result<Q, AllocError> {
            let current_count = self.heuristic_byte_count.get();
            let new_count = allocation_delta(
                current_count,
                old_layout_aligned_blocksize,
                new_layouts_total_aligned_blocksize,
                self.maximum.get(),
            )?;
            let output = operation(&self.inner_allocator)?;
            self.heuristic_byte_count.set(new_count);
            Ok(output)
        }

        /// Adjust the soft limit on memory usage.
        ///
        /// If you decrease the limit, then any allocator operation that increases the overall
        /// amount of allocated memory (as recorded by the heuristic) will error. It does, however,
        /// allow shrinking even if that requires temporary allocation.
        ///
        /// Returns the old limit.
        #[inline]
        pub fn adjust_limit(&self, new_limit: usize) -> usize {
            self.maximum.replace(new_limit)
        }

        /// Get the current limit on memory usage.
        #[inline]
        pub fn get_current_limit(&self) -> usize {
            self.maximum.get()
        }
    }

    /// SAFETY - All of these forward to inner functions. which satisfy the function requirements,
    /// and similar.
    #[allow(unsafe_op_in_unsafe_fn)]
    unsafe impl<A: ExtendedAllocator> ExtendedAllocator for &'_ Dynamic<A> {
        #[inline]
        fn allocate(
            &self,
            layout: core::alloc::Layout,
        ) -> Result<core::ptr::NonNull<[u8]>, AllocError> {
            // Use the provided alignment as a heuristic
            let min_block_size = aligned_minimum_block_size(layout);
            self.allocator_operation(0, min_block_size, |a| a.allocate(layout))
        }

        #[inline]
        unsafe fn deallocate(&self, ptr: core::ptr::NonNull<u8>, layout: core::alloc::Layout) {
            // Fitting means this is only approximate, but it is unfortunately always an
            // overestimate :/
            let min_allocated_block_size = aligned_minimum_block_size(layout);
            self.allocator_operation(min_allocated_block_size, 0, |a| {
                a.deallocate(ptr, layout);
                Ok(())
            })
            .expect("Allocation operation guarunteed to succeed with only deallocation occurring")
        }

        #[inline]
        fn allocate_zeroed(
            &self,
            layout: core::alloc::Layout,
        ) -> Result<core::ptr::NonNull<[u8]>, AllocError> {
            // Use the provided alignment as a heuristic
            let min_block_size = aligned_minimum_block_size(layout);
            self.allocator_operation(0, min_block_size, |a| a.allocate_zeroed(layout))
        }

        #[inline]
        unsafe fn grow(
            &self,
            ptr: core::ptr::NonNull<u8>,
            old_layout: core::alloc::Layout,
            new_layout: core::alloc::Layout,
        ) -> Result<core::ptr::NonNull<[u8]>, AllocError> {
            debug_assert!(
                new_layout.size() >= old_layout.size(),
                "`new_layout.size()` must be greater than or equal to `old_layout.size()`"
            );

            let min_dealloc_block_size = aligned_minimum_block_size(old_layout);
            let min_realloc_block_size = aligned_minimum_block_size(new_layout);
            self.allocator_operation(min_dealloc_block_size, min_realloc_block_size, |a| {
                a.grow(ptr, old_layout, new_layout)
            })
        }

        #[inline]
        unsafe fn grow_zeroed(
            &self,
            ptr: core::ptr::NonNull<u8>,
            old_layout: core::alloc::Layout,
            new_layout: core::alloc::Layout,
        ) -> Result<core::ptr::NonNull<[u8]>, AllocError> {
            debug_assert!(
                new_layout.size() >= old_layout.size(),
                "`new_layout.size()` must be greater than or equal to `old_layout.size()`"
            );

            let min_dealloc_block_size = aligned_minimum_block_size(old_layout);
            let min_realloc_block_size = aligned_minimum_block_size(new_layout);
            self.allocator_operation(min_dealloc_block_size, min_realloc_block_size, |a| {
                a.grow_zeroed(ptr, old_layout, new_layout)
            })
        }

        #[inline]
        unsafe fn shrink(
            &self,
            ptr: core::ptr::NonNull<u8>,
            old_layout: core::alloc::Layout,
            new_layout: core::alloc::Layout,
        ) -> Result<core::ptr::NonNull<[u8]>, AllocError> {
            debug_assert!(
                new_layout.size() <= old_layout.size(),
                "`new_layout.size()` must be smaller than or equal to `old_layout.size()`"
            );

            let min_dealloc_block_size = aligned_minimum_block_size(old_layout);
            let min_realloc_block_size = aligned_minimum_block_size(new_layout);
            self.allocator_operation(min_dealloc_block_size, min_realloc_block_size, |a| {
                a.shrink(ptr, old_layout, new_layout)
            })
        }

        #[inline(always)]
        fn by_ref(&self) -> &Self
        where
            Self: Sized,
        {
            self
        }

        #[inline]
        unsafe fn split(
            &self,
            old_allocation: core::ptr::NonNull<u8>,
            old_layout: core::alloc::Layout,
            new_layout_start: core::alloc::Layout,
            new_layout_end: core::alloc::Layout,
        ) -> Result<(core::ptr::NonNull<[u8]>, core::ptr::NonNull<[u8]>), AllocError> {
            debug_assert!(
                new_layout_start.size() + new_layout_end.size() <= old_layout.size(),
                "`new_layout_start.size() + new_layout_end.size()` must be less than or equal to `old_layout.size()`"
            );

            let dealloc_heuristic = aligned_minimum_block_size(old_layout);
            let realloc_heuristic = aligned_minimum_block_size(new_layout_start)
                + aligned_minimum_block_size(new_layout_end);
            self.allocator_operation(dealloc_heuristic, realloc_heuristic, |a| {
                a.split(old_allocation, old_layout, new_layout_start, new_layout_end)
            })
        }
    }
}

/// # Hard Restrictions
/// Hard restrictions on memory usage - that is, ensuring that the total allocated bytes never
/// exceed a certain amount - requires some cooperation from the allocator.
///
/// In particular - allocator_api style allocators, by default, are very flexible in the amount of
/// memory they provide for a given input [`Layout`], and in how much memory is referred-to by a
/// given pointer and layout when deallocating. The information is not necessarily embedded in the
/// allocator.
///
/// To solve this problem, it is necessary for the allocator to be *instrumentable* - to provide
/// the functionality to determine the exact amount of memory that will be, or currently is,
/// allocated for a specific type of layout and at a specific pointer.
pub mod hard {
    use crate::{instrument::*, AllocError, ExtendedAllocator};
}

// core-alloc
// Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
