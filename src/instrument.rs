//! Provides tools to enable strict instrumentation of an allocator, including any
//! dynamically-allocated bookkeeping information.
//!
//! # Allocator Model
//! Rust allocators in general - and our allocators as a consequence - require that combined
//! allocation operations like `split` and `grow` do not invalidate the initial provided memory
//! block on failure.
//!
//! By default - and often even in practise as a fallback path - these allocation functions are
//! composed of multiple simpler `deallocate` or `allocate` operations which for non-tracking
//! allocators are arranged such that failure does not invalidate the original passed in memory
//! block.
//!
//! When tracking and restricting allocations, therefore, this means that any initial allocation
//! cannot succeed in the inner allocator, but fail *afterwards* due to the observed allocation
//! being too large, as the inner operation will have already invalidated the internal memory
//! block.
//!
//! In practise, this means that to perform hard memory restriction, this crate has to know exactly
//! how much new allocated memory - both for bookkeeping and actual allocations - will be used and
//! freed in each operation *before* the operation is actually attempted. It also must know how
//! much temporary excess allocation is present within the operation.
//!
//! This is difficult to predict for most allocators, because they carry important internal state
//! which means that the memory used for and allocated for a given request may change depending on
//! some previous combination of allocations or deallocations. To make APIs accounting for this
//! ergonomic, we introduce the concept of *hypothetical allocation events*.
//!
//! ## Events
//! To account for internal state changes, this module provides a mechanism for an allocator to
//! encode the exact state changes that *would-have-been* made after some series of allocations,
//! deallocations, grows, shrinks, splits, etc., without *actually* performing them - `Delta`s.
//!
//! Then, an allocator can build it's memory block assignments with an additional `&[Delta]` delta
//! parameter, which performs the calculation with the current state *as-if* the additional changes
//! to internal state had been applied.
//!
//! The APIs for instrumentation also take a list of "as-if" deltas for each event, allowing
//! ergonomic implementation of arbitrary hypothetical state changes without actually applying
//! those changes in the the internal state.

use crate::{
    alloc_error,
    slicetree::{AsSliceTreeFromPair, AsSliceTreeFromSlice, SliceTree},
    AllocError, ExtendedAllocator,
};


/// Like ? but has no From/Into conversion, so it's safe for const contexts.
macro_rules! const_safe_try {
    ($expr:expr) => {
        match $expr {
            Ok(v) => v,
            Err(e) => return Err(e),
        }
    };
}

/// Safely subtract two usizes into an isize returning [`AllocError`] on:
/// * Conversion error (usizes out of range of isize).
/// * any overflow
///
/// This is publically provided as a utility for implementors where it is often needed to do
/// this sort of subtraction.
#[inline(always)]
pub const fn safesub(lhs: usize, rhs: usize) -> Result<isize, AllocError> {
    #[inline(always)]
    const fn maybe_conv(v: usize) -> Result<isize, AllocError> {
        if v <= isize::MAX as usize {
            Ok(v as isize)
        } else {
            Err(alloc_error())
        }
    }
    match const_safe_try!(maybe_conv(lhs)).checked_sub(const_safe_try!(maybe_conv(rhs))) {
        Some(v) => Ok(v),
        None => Err(alloc_error()),
    }
}

/// Checked add of a usize to an isize
/// Replacement for the unstable feature that is stable, and returns [`AllocError`] on failure.
#[inline(always)]
pub const fn checkedadd(lhs: isize, rhs: usize) -> Result<isize, AllocError> {
    // Taken from stdlib
    #[inline]
    const fn overflowing_add_unsigned(lhs: isize, rhs: usize) -> (isize, bool) {
        let rhs = rhs as isize;
        let (res, overflowed) = lhs.overflowing_add(rhs);
        (res, overflowed ^ (rhs < 0))
    }
    let (res, did_overflow) = overflowing_add_unsigned(lhs, rhs);
    if did_overflow {
        Err(alloc_error())
    } else {
        Ok(res)
    }
}

/// Safely add a usize and an isize, preventing wrapping etc. and returning [`AllocError`] on
/// failure of any kind. It will also saturate on the bottom - i.e. if the result would be < 0, it
/// returns zero - which is good for obtaining the amount of "extra" memory from a combination of
/// deltas.
///
/// This is publically provided as a utility for implementors where it is often needed to do this
/// sort of addition safely.
#[inline(always)]
pub const fn zero_saturating_safeadd(lhs: usize, rhs: isize) -> Result<usize, AllocError> {
    // Note we copypaste from the stdlibs `saturating_add_signed` here, because it's just not quite
    // stabilised yet.
    // Then we identify when it overflowed on the top end and error instead of returning usize::MAX
    Ok({
        let (res, overflow) = lhs.overflowing_add(rhs as usize);
        if overflow == (rhs < 0) {
            res
        } else if overflow {
            return Err(alloc_error());
        } else {
            0
        }
    })
}

/// Many allocators have *order-dependent allocations*. That is, their allocation results and
/// more specifically internal memory usage and provided block sizes may be conditional on
/// pre-accumulated state. This module enables ergonomically accounting for this fact when
/// defining instrumented allocators by abstracting upon hypothetical future allocation "events".
///
/// The rust allocation API requires that allocations be atomic. When providing defaults for
/// combined operations, this aspect combined with the uncertain ordering can cause difficulty,
/// because an allocation that may succeed in an initial state could fail or use a different
/// amount of memory after the first of a collection of allocation events.
///
/// We, therefore, create an API that enables the calculation of total state change for an
/// allocator for arbitrary events in sequence - though only three are used in practise.
pub mod events {
    #[cfg(doc)]
    use crate::ExtendedAllocator;
    use core::{alloc::Layout, ptr::NonNull};

    #[derive(Clone, Copy, Debug, PartialEq, Eq)]
    /// Represents the amount of memory that would be consumed in a simple allocation event.
    pub struct AllocatingEventResourceChanges {
        /// Exact amount of memory that would be allocated - including alignment bytes and minimum
        /// chunk size bytes - for the allocation.
        ///
        /// In debug mode, this is checked against the size of any returned regions.
        pub allocated_bytes: usize,

        /// Exact amount of memory *dynamically allocated* for bookkeeping. Note that if your
        /// bookkeeping data is all static, there is no need to supply this (just make it
        /// zero). This also must include padding/alignment/etc.
        pub allocated_bookkeeping_bytes: usize,
    }

    impl AllocatingEventResourceChanges {
        #[inline]
        /// Total amount of allocated bytes, both for bookkeeping and internal allocation.
        ///
        ///
        pub const fn total_allocated_bytes(&self) -> usize {
            self.allocated_bytes + self.allocated_bookkeeping_bytes
        }
    }

    #[derive(Clone, Copy, Debug)]
    /// Event signalling a single allocation would-occur.
    pub struct AllocationEvent {
        /// The layout for which an allocation is requested.
        pub requested_layout: Layout,
        /// If the allocation should be zeroed or not
        pub zeroed_op: bool,
    }

    #[derive(Clone, Copy, Debug, PartialEq, Eq, Default)]
    /// Represents the amount of memory that would be released in a single deallocation
    ///
    /// The [`Default::default`] value essentially is the case where no deallocation - or a
    /// zero-sized deallocation with no bookkeeping overhead - occurs.
    pub struct DeallocatingEventResourceChanges {
        /// Exact amount of memory that would be deallocated and invalidated by this
        /// deallocation operation.
        pub deallocated_bytes: usize,
        /// Exact amount of bookkeeping memory that would be freed for reuse - in particular,
        /// this solely pertains to bookkeeping that does not have statically determined memory
        /// usage.
        pub freed_bookkeeping_bytes: usize,
    }

    impl DeallocatingEventResourceChanges {
        #[inline]
        /// The total amount of dynamically-allocated bytes - bookkeeping and for actual memory
        /// - that would be freed up in this event.
        pub const fn total_deallocated_bytes(&self) -> usize {
            self.deallocated_bytes + self.freed_bookkeeping_bytes
        }
    }

    /// Event signalling a single de-allocation would-occur
    #[derive(Clone, Copy, Debug)]
    pub struct DeallocationEvent {
        /// The pointer that would-be deallocated.
        pub memory_block: NonNull<u8>,
        /// The layout of the type to be deallocated. Must fit the memory block denoted by [`DeallocationEvent::memory_block`]
        pub fitting_layout: Layout,
    }

    #[derive(Clone, Copy, Debug, PartialEq, Eq)]
    /// Represents the amount of memory that would be grown and dealloced by a growing operation,
    /// as well as the maximum intermediary consumption.
    ///
    /// In particular, default implementations of `grow`/`grow_zeroed` will hold open the
    /// initial allocation until all it's contents are copied to a new allocation.
    pub struct GrowingEventResourceChanges {
        /// Any new allocation that has occurred and it's amount. Note that this is checked
        /// against the size of the output slice in debug mode.
        pub new_allocated_bytes: usize,

        /// Change in any dynamically allocated bookkeeping state's total bytes, by the end of
        /// the operatio n.
        pub bookkeeping_bytes_delta: isize,

        /// However many bytes were deallocated in the process. This should be the total
        /// allocated bytes of the input block, identical to the length of the outputted memory
        /// block initially allocated.
        pub deallocated_bytes: usize,

        /// Defining the maximum allocation extent is difficult.
        ///
        /// This variable represents how much excess memory may be allocated by the allocator
        /// temporarily for the operation.
        ///
        /// In particular, it represents the maximum amount of how much memory would be used in the
        /// case where the internal function blindly deallocated the initial argument
        /// immediately, and allocated the results immediately without any data preservation.
        ///
        /// This should include dynamic bookkeeping bytes as well.
        pub maximum_relative_excess_dynamic_bytes: usize,
    }

    #[derive(Copy, Clone, Debug)]
    /// Represents an in-the-future event for *growing* an existing allocation.
    ///
    /// The values are restricted to the same constraints as those in [`ExtendedAllocator::grow`] and
    /// [`ExtendedAllocator::grow_zeroed`]
    pub struct GrowEvent {
        /// Pointer to existing memory block
        pub memory_block: NonNull<u8>,
        /// Layout that "fits" the memory block :) and similar.
        pub old_layout: Layout,
        /// The new, desired layout to grow into.
        pub new_layout: Layout,
        /// Whether the operation is zeroed or not
        pub zeroed_op: bool,
    }

    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    /// Represents the amount of memory that would be shrunk and deallocated by a shrinking
    /// operation, as well as the maximum excess memory consumption beyond perfect efficiency
    /// of memory usage.
    ///
    /// In particular, default implementations of `shrink` will hold open the original
    /// allocation until the new allocation has succeeded and the old memory can be copied.
    /// This means that partway through the function, more bytes are allocated than in the
    /// middle.
    pub struct ShrinkingEventResourceChanges {
        /// The amount of bytes now allocated for the shrunk chunk, including alignment bytes,
        /// minimum allocation size padding, etc.
        ///
        /// This is checked against the final output slice length in debug mode.
        pub new_allocated_bytes: usize,

        /// Change in any dynamically allocated bookkeeping bytes induced by this operation.
        /// Note that this should not include bookkeeping part of a statically known and
        /// statically sized memory chunk - it's for dynamic bookkeeping only.
        ///
        /// It should include padding/alignment/etc. bytes.
        pub bookkeeping_bytes_delta: isize,

        /// However many bytes were deallocated in the process. This should be the total
        /// allocated bytes of the input block, identical to the length of the outputted memory
        /// block initially allocated.
        pub deallocated_bytes: usize,

        /// Defining the maximum allocation extent is difficult.
        ///
        /// This variable represents how much excess memory may be allocated by the allocator
        /// temporarily for the operation.
        ///
        /// In particular, it represents the maximum amount of how much memory would be used in the
        /// case where the internal function blindly deallocated the initial argument
        /// immediately, and allocated the results immediately without any data preservation.
        ///
        /// This should include dynamic bookkeeping bytes as well.
        pub maximum_relative_excess_dynamic_bytes: usize,
    }

    #[derive(Clone, Copy, Debug)]
    /// Represents an in-the-future event for shrinking a given memory block from the 'fitting'
    /// old layout to the 'fitting' new layout.
    ///
    /// The values are restricted by the same constraints as parameters to
    /// [`ExtendedAllocator::shrink`], except applied to the allocator *as-if* several other
    /// hypothetical changes were made to it's internal state.
    pub struct ShrinkEvent {
        /// Pointer to existing memory block
        pub memory_block: NonNull<u8>,
        /// Old layout that "fits" the memory block :)
        pub old_layout: Layout,
        /// The new, desired layout to shrink into
        pub new_layout: Layout,
    }

    #[derive(Clone, Copy, Debug, PartialEq, Eq)]
    /// Represents the amount of memory that would be allocated and equivalently deallocated
    /// for an operation to split a single allocation.
    pub struct SplitEventResourceChanges {
        /// The amount of bytes allocated at the end of  the process for the
        /// start chunk returned, including alignment and padding for minimum allocation
        /// blocksize.
        ///
        /// This is checked against actual allocation size in debug mode.
        pub new_allocated_start_bytes: usize,
        /// The amount of bytes allocated at the end of the process for the end chunk returned,
        /// including alignment and padding bytes.
        ///
        /// Checked against actual allocation size in debug mode.
        pub new_allocated_end_bytes: usize,
        /// Overall change in the amount of dynamically-allocated bookkeeping bytes that would
        /// occur in the splitting operation.
        pub bookkeeping_bytes_delta: isize,
        /// The amount of bytes that will be deallocated by the end of the process. Bookkeeping
        /// effects should be included in [`Self::bookkeeping_bytes_delta`]
        pub deallocated_bytes: usize,

        /// Defining the maximum allocation extent is difficult.
        ///
        /// This variable represents how much excess memory may be allocated by the allocator
        /// temporarily for the operation.
        ///
        /// In particular, it represents the maximum amount of how much memory would be used in the
        /// case where the internal function blindly deallocated the initial argument
        /// immediately, and allocated the results immediately without any data preservation.
        ///
        /// This should include dynamic bookkeeping bytes as well.
        pub maximum_relative_excess_dynamic_bytes: usize,
    }

    /// A hypothetical future splitting of an allocation. In particular, the values inside this
    /// follow the same restrictions as the parameters of [`ExtendedAllocator::split`].
    pub struct SplitEvent {
        /// Pointer to existing memory block
        pub memory_block: NonNull<u8>,
        /// Layout to fit old memory block
        pub old_layout: Layout,
        /// Layout for the first chunk of the new allocations
        pub new_layout_start: Layout,
        /// Layout for the second chnk of the new allocations
        pub new_layout_end: Layout,
    }
}

/// Allocator that can perfectly instrument the exact amount of dynamically allocated memory it
/// uses.
///
/// Users of allocators that implement this trait may rely on the fact that any count provided
/// is the exact amount of dynamically allocated memory - including padding, overallocations, bookkeeping, etc. used by
/// allocations, unless they *explicitly* ignore some aspects like bookkeeping.
///
/// # Safety
/// Implementing this trait safely requires that your allocator can determine exactly how much
/// memory - both for actual data and for dynamic bookkeeping - that it will clear or consume
/// *before* actually performing the allocation.
///
/// This is because rust allocators require their
/// operations to be *atomic* - that is, they either fail and leave the original memory valid,
/// or succeed. As such, other than `debug_assert!`, this can't check for validity and fail an
/// allocation *after* it has already gone through a whole process, it must know *beforehand*.
///
/// Your allocator *must* ensure the numbers are totally accurate. `debug_assert!` should help
/// with testing, but any allocator implementing this may be relied upon in hardened or deeply
/// embedded environments to prevent undue consumption of memory.
///
/// Inputs to all functions here meet (and must meet) the same criteria as those of
/// [`ExtendedAllocator`] - w.r.t to "fitting" of blocks and similar. Generally speaking, if you provide a
/// custom implementation of an [`ExtendedAllocator`] function, you should also provide a
/// custom implementation of the corresponding [`InstrumentedAllocator`] function to avoid
/// unnecessary allocation failures if you have a more efficient implementation.
///
/// If you don't, then the default implementations here correspond directly to the default
/// implementations in [`ExtendedAllocator`], using the existing implementations of default
/// methods to determine how to calculate the exact quantities of memory allocated or
/// deallocated.
///
/// The trait provides an explicit mechanism to account for internal state changes in the form of
/// the [`InstrumentedAllocator::Delta`] type that can encode what internal state changes
/// would-have occured in some arbitrary previous series of events.
///
/// # Inlining?
/// Crate users may wonder at the aggressive `#[inline]` annotations on the default implementations
/// of these functions. 
/// 
/// The reason for such is that *most of the time*, this trait is used to
/// restrict total memory consumption of an allocator. In particular, the specific values of all
/// the fields of resource consumption that are returned by each function are usually just added
/// and subtracted together, unless in debug mode.
///
/// The aggressive inlining is in the hopes that the compiler will optimise values to just be sums
/// in release mode. Most operations on the allocator will at most be 3 or 4 entirely fixed
/// compiletime-determined events, and the compiler should be able to see through any memory usage
/// pre-evaluation calls, identify the total extra memory, and just do a direct comparison with
/// maybe a little calculation.
/// 
/// Providing the full named values rather than just an overall delta is important though. It is important for:
/// * Allowing fuzzing of predicted vs actual usage to ensure consistency in implementation, where
///   the separation between internal bookkeeping allocation and actual allocated chunk length is important. 
///   If they were combined then any dynamic bookkeeping would render instrumentation of actual
///   allocations for testing purposes impossible.
/// * Keeping track of temporary per-operation excess usage of allocations vs longer term usage of
///   memory.
/// * The separation actually makes implementing the numbers correctly far more simple. The concept
///   of temporary excess allocation is itself rather complex.
#[allow(unsafe_op_in_unsafe_fn)]
pub unsafe trait InstrumentedAllocator: ExtendedAllocator {
    /// Documents the change due to a single allocator event.
    ///
    /// Your allocator must be able to handle an arbitrary number of deltas and apply them to
    /// it's internal state in a partial/accumulating form to work on potential future
    /// allocations.
    ///
    /// It is *recommended* that you use a single implementation to determine any particular
    /// changes that should be made, and provide an `&[Delta]` argument to it, for deduplication of
    /// implementation.
    type Delta;

    /// Function that calculates the expected resource consumption of the allocator after it
    /// provided the previous collection of state changes for other events.
    ///
    /// In particular, this must provide *precise* amounts of memory usage.
    ///
    /// The returned values are as follows:
    /// * The delta is what extra internal changes would be made for an allocator in this
    ///   hypothetical future allocation event.
    /// * The resource change structure indicates how much every single resource used in the
    ///   allocator would change.
    /// * The error case is for when the allocator would not be able to perform this allocation.
    ///
    /// # Safety
    /// The parameters in the allocation event have the exact same restrictions as described in
    /// [`ExtendedAllocator::allocate`], except with the caveat that they apply to the allocator *as-if
    /// the provided delta states had been applied to it's internal state*. This absolutely
    /// cannot invalidate or actually modify internal state in any regard.
    unsafe fn next_hypothetical_allocation<'q>(
        &self,
        previous_changes: SliceTree<'q, &'q Self::Delta>,
        next_allocation: events::AllocationEvent,
    ) -> Result<(Self::Delta, events::AllocatingEventResourceChanges), AllocError>;

    /// Function that adds to an arbitrary chain of events with a hypothetical
    /// deallocation.
    ///
    /// Results:
    /// * The further internal state changes that would occur after this deallocation
    /// * How much resources were freed by performing this deallocation.
    ///
    /// # Safety
    /// The parameters in the deallocator event have the same restrictions as in
    /// [`ExtendedAllocator::deallocate`], except applying to the allocator *as-if* the
    /// provided deltas were applied to it's internal state.
    ///
    /// Deallocation is also infallible internally, which is why we provide no option for
    /// error.
    unsafe fn next_hypothetical_deallocation<'q>(
        &self,
        previous_changes: SliceTree<'q, &'q Self::Delta>,
        finalising_deallocation: events::DeallocationEvent,
    ) -> (Self::Delta, events::DeallocatingEventResourceChanges);

    /// Function that adds to an arbitrary chain of events for a growing operation.
    ///
    /// Note that the default implementation only requires two deltas - one for an allocation and
    /// one for a deallocation. This means that for our API we only provide a maximum of two deltas
    /// as a result
    ///
    /// # Safety
    /// The parameters to this function have the same restrictions as in
    /// [`ExtendedAllocator::grow`]/[`ExtendedAllocator::grow_zeroed`], except *as-if* the internal
    /// allocator had the provided deltas applied to it's internal state.
    #[inline]
    unsafe fn next_hypothetical_grow<'q>(
        &self,
        previous_changes: SliceTree<'q, &'q Self::Delta>,
        next_grow: events::GrowEvent,
    ) -> Result<
        (
            arrayvec::ArrayVec<Self::Delta, 2>,
            events::GrowingEventResourceChanges,
        ),
        AllocError,
    > {
        // default implementation allocates the new thing before copying and then eventually
        // deallocating.
        let (first_delta, new_alloc_resources) = self.next_hypothetical_allocation(
            previous_changes,
            events::AllocationEvent {
                requested_layout: next_grow.new_layout,
                zeroed_op: next_grow.zeroed_op,
            },
        )?;
        let (second_delta, dealloc_freed_resources) = self.next_hypothetical_deallocation(
            (&previous_changes, [&first_delta].as_slice_tree()).as_slice_tree(),
            events::DeallocationEvent {
                memory_block: next_grow.memory_block,
                fitting_layout: next_grow.old_layout,
            },
        );

        // The default implementation leaves the allocation from the start open until after the new
        // memory is copied-to.
        let resources = events::GrowingEventResourceChanges {
            new_allocated_bytes: new_alloc_resources.allocated_bytes,
            deallocated_bytes: dealloc_freed_resources.deallocated_bytes,
            bookkeeping_bytes_delta: safesub(
                new_alloc_resources.allocated_bookkeeping_bytes,
                dealloc_freed_resources.freed_bookkeeping_bytes,
            )?,
            // The original allocation remains open until the end, which means it is excess beyond
            // "immediate deallocation and new allocations"
            maximum_relative_excess_dynamic_bytes: dealloc_freed_resources
                .total_deallocated_bytes(),
        };
        Ok(([first_delta, second_delta].into(), resources))
    }

    /// Function that adds to an arbitrary chain of hypothetical events, a shrink operation.
    ///
    /// The default implementation of [`ExtendedAllocator::shrink`] only requires two deltas
    /// - one for an allocation and one for a deallocation. As such, this API only allows for a
    /// maximum of two deltas returned.
    ///
    /// # Safety
    /// The parameters for this function have the same safety requirements as the parameters for
    /// [`ExtendedAllocator::shrink`], except *as-if* the series of deltas provided had been
    /// applied to the allocator.
    #[inline]
    unsafe fn next_hypothetical_shrink<'q>(
        &self,
        previous_changes: SliceTree<'q, &'q Self::Delta>,
        next_shrink: events::ShrinkEvent,
    ) -> Result<
        (
            arrayvec::ArrayVec<Self::Delta, 2>,
            events::ShrinkingEventResourceChanges,
        ),
        AllocError,
    > {
        // Default implementation allocates the new thing before copying and then deallocating the
        // old
        let (first_delta, new_alloc_resources) = self.next_hypothetical_allocation(
            previous_changes,
            events::AllocationEvent {
                requested_layout: next_shrink.new_layout,
                zeroed_op: false,
            },
        )?;

        let (second_delta, freed_dealloc_resources) = self.next_hypothetical_deallocation(
            (&previous_changes, [&first_delta].as_slice_tree()).as_slice_tree(),
            events::DeallocationEvent {
                fitting_layout: next_shrink.old_layout,
                memory_block: next_shrink.memory_block,
            },
        );

        // Default operation leaves start memory open until the end of the operation
        //
        // Therefore, it is excess from immediate-dealloc and then alloc of new.
        let resources = events::ShrinkingEventResourceChanges {
            new_allocated_bytes: new_alloc_resources.allocated_bytes,
            deallocated_bytes: freed_dealloc_resources.deallocated_bytes,
            bookkeeping_bytes_delta: safesub(
                new_alloc_resources.allocated_bookkeeping_bytes,
                freed_dealloc_resources.freed_bookkeeping_bytes,
            )?,
            maximum_relative_excess_dynamic_bytes: freed_dealloc_resources
                .total_deallocated_bytes(),
        };

        Ok(([first_delta, second_delta].into(), resources))
    }

    /// Function that adds to a chain of hypothetical events with a *split* operation.
    ///
    /// The default implementation of [`ExtendedAllocator::split`] only requires three deltas,
    /// however this operation may be more complex in other allocators, and so the API provides an
    /// allowance of 5 deltas to encode internal state changes required for a splitting operation.
    ///
    /// # Safety
    /// The parameters of this function have all the same safety requirements as
    /// [`ExtendedAllocator::split`], except *as-if* the extra processes have been applied to the
    /// allocator.
    #[inline]
    unsafe fn next_hypothetical_split<'q>(
        &self,
        previous_changes: SliceTree<'q, &'q Self::Delta>,
        next_split: events::SplitEvent,
    ) -> Result<
        (
            arrayvec::ArrayVec<Self::Delta, 5>,
            events::SplitEventResourceChanges,
        ),
        AllocError,
    > {
        // Default implementation allocates for the second chunk of the layout
        let (first_delta, end_layout_resource_changes) = self.next_hypothetical_allocation(
            previous_changes,
            events::AllocationEvent {
                requested_layout: next_split.new_layout_end,
                zeroed_op: false,
            },
        )?;

        // Then use the shrinking.
        let (mut second_deltas, shrinking_start_layout_resource_changes) = self
            .next_hypothetical_shrink(
                (&previous_changes, [&first_delta].as_slice_tree()).as_slice_tree(),
                events::ShrinkEvent {
                    memory_block: next_split.memory_block,
                    old_layout: next_split.old_layout,
                    new_layout: next_split.new_layout_start,
                },
            )?;

        // The only excess usage can be in shrink, as all allocations made in the default impl of
        // this function are returned at the end.
        let resources = events::SplitEventResourceChanges {
            new_allocated_start_bytes: shrinking_start_layout_resource_changes.new_allocated_bytes,
            new_allocated_end_bytes: end_layout_resource_changes.allocated_bytes,
            bookkeeping_bytes_delta: checkedadd(
                shrinking_start_layout_resource_changes.bookkeeping_bytes_delta,
                end_layout_resource_changes.allocated_bookkeeping_bytes,
            )?,
            // The only excess here is any present in the shrinking operation. If the shrinking
            // However, if the shrinking operation does not have excess then we still hold open
            // temporary allocations. In particular, whatever is *freed* in the shrinking operation
            // still remains open until the shrinking operation occurs.
            //
            // Therefore, the maximum excess is the maximum between the amount freed by the
            // shrinking operation - as that is held open/allocated until the shrinking operation -
            // and the temporary excess of the shrinking operation itself, which is how much extra
            // is held open of that compared to immediate deallocation and new allocation.
            maximum_relative_excess_dynamic_bytes: usize::max(
                shrinking_start_layout_resource_changes.deallocated_bytes,
                shrinking_start_layout_resource_changes.maximum_relative_excess_dynamic_bytes,
            ),
            deallocated_bytes: shrinking_start_layout_resource_changes.deallocated_bytes,
        };

        let mut deltas = arrayvec::ArrayVec::<Self::Delta, 5>::new();
        deltas.push(first_delta);
        for v in second_deltas.drain(..) {
            deltas.push(v)
        }
        Ok((deltas, resources))
    }

}

#[cfg(feature = "fuzzing")]
/// Utilities to better enable fuzzing of instrumented allocators.
///
/// This enables the generation of an arbitrary sequence of allocation events, and enables dividing
/// them up into chunks to test self-consistency between carried over internal state
/// ([`InstrumentedAllocator::Delta`] when exploring a potential future collection of operations,
/// and the actual implementation of those operations.
///
/// This uses the structures in [`crate::fuzzing`] as a basis, and allows for checking that any
/// allocations are accurate to that predicted by the implementation of [`InstrumentedAllocator`]
///
/// It does this, as well, with arbitrary numbers of hypothetical state changes for bulk atomic
/// operation capabilities - that is, succeed-and-invalidate or fail-and-leave-valid as the only two possible 
/// options. 
///
/// # Note on Hypothetical Allocations
/// Because hypothetical allocation calculations typically require a realised memory pointer as
/// input unless they are allocations, hypothetical calculations can only work for as long as some
/// sequence of allocator operations fit the following criteria:
/// * They depend on nothing (hypothetical allocations)
/// * They depend on an existing allocation operation that has already-happened - i.e. 
///   there is a real `NonNull<u8>` already-existing for the memory block.
///
/// Notably, we can't manage hypothetical operations where one hypothetical operation would depend
/// on the actual realised output memory block of another.
pub mod fuzzing {
    /*
    use arbitrary::Arbitrary;
    use crate::fuzzing as alloc_fuzz;
    */

}

// core-alloc
// Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
