//! Provide a way to pass around arbitrary-depth "trees" of continuous slices that can
//! be appended to by referencing the preexisting tree.
//!
//! These are designed for the case where you want to join slices together without needing
//! to dynamically allocate.

use core::ops::Index;

/// Iterator over a slice tree.
///
/// This requires that the lengths in [`SliceTree::Branch`] variants are accurate, as the iterator
/// uses them to traverse the tree. If you solely use [`Into`] or [`AsSliceTree`] trait methods to
/// construct slice trees, this will always be the case.
#[derive(Debug)]
pub struct SliceTreeIterator<'s, Item> {
    inner_tree: SliceTree<'s, Item>,
    index: usize,
}

impl<'s, Item> Iterator for SliceTreeIterator<'s, Item> {
    type Item = &'s Item;

    fn next(&mut self) -> Option<Self::Item> {
        let ret = self.inner_tree.get(self.index);
        self.index += 1;
        ret
    }
}

impl<'s, Item> IntoIterator for SliceTree<'s, Item> {
    type Item = &'s Item;

    type IntoIter = SliceTreeIterator<'s, Item>;

    fn into_iter(self) -> Self::IntoIter {
        SliceTreeIterator {
            inner_tree: self,
            index: 0usize,
        }
    }
}

impl<'s, 'q: 's, Item> IntoIterator for &'s SliceTree<'q, Item> {
    type Item = &'q Item;

    type IntoIter = SliceTreeIterator<'q, Item>;

    #[inline]
    fn into_iter(self) -> Self::IntoIter {
        (*self).into_iter()
    }
}

impl<'q, Item> Index<usize> for SliceTree<'q, Item> {
    type Output = Item;

    #[inline]
    fn index(&self, index: usize) -> &Self::Output {
        self.get(index).expect("Out of range index")
    }
}

/// Simple extension trait to disabiguate target types easily.
///
/// Note that due to conflicts with crates potentially implementing both AsRef<[Item]>, and
/// AsRef<SliceTree> we have [`AsSliceTreeFromSlice`] and [`AsSliceTreeFromPair`] with a trait
/// method of the same name on both
pub trait AsSliceTreeFromSlice<'q, Item> {
    /// Convert our selves into a slice tree.
    fn as_slice_tree<'s>(&'s self) -> SliceTree<'s, Item>
    where
        'q: 's;
}

/// Simple extension trait to disabiguate target types easily.
///
/// Note that due to conflicts with crates potentially implementing both AsRef<[Item]>, and
/// AsRef<SliceTree> we have [`AsSliceTreeFromSlice`] and [`AsSliceTreeFromPair`] with a trait
/// method of the same name on both
pub trait AsSliceTreeFromPair<'q, Item> {
    /// Convert our selves into a slice tree.
    fn as_slice_tree<'s>(&'s self) -> SliceTree<'s, Item>
    where
        'q: 's;
}

impl<'q, Item, T: AsRef<[Item]> + 'q> AsSliceTreeFromSlice<'q, Item> for T {
    #[inline]
    fn as_slice_tree<'s>(&'s self) -> SliceTree<'s, Item>
    where
        'q: 's,
    {
        SliceTree::from_slice(self.as_ref())
    }
}

impl<'q, Item: 'q, T: AsRef<SliceTree<'q, Item>>, Q: AsRef<SliceTree<'q, Item>>>
    AsSliceTreeFromPair<'q, Item> for (T, Q)
{
    #[inline]
    fn as_slice_tree<'s>(&'s self) -> SliceTree<'s, Item>
    where
        'q: 's,
    {
        SliceTree::from_branches((self.0.as_ref(), self.1.as_ref()))
    }
}

/// Like [`AsSliceTreeFromPair`] but builds from a pair of references rather than a references of
/// pairs
pub trait IntoSliceTreeFromRefPair<'q, Item> {
    /// Convert our selves into a slice tree
    fn into_slice_tree(self) -> SliceTree<'q, Item>;
}

impl<'q, Item> IntoSliceTreeFromRefPair<'q, Item>
    for (&'q SliceTree<'q, Item>, &'q SliceTree<'q, Item>)
{
    #[inline]
    fn into_slice_tree(self) -> SliceTree<'q, Item> {
        SliceTree::from_branches((self.0, self.1))
    }
}

impl<'q, Item> IntoSliceTreeFromRefPair<'q, Item>
    for (&'q mut SliceTree<'q, Item>, &'q mut SliceTree<'q, Item>)
{
    #[inline]
    fn into_slice_tree(self) -> SliceTree<'q, Item> {
        SliceTree::from_branches((&*self.0, &*self.1))
    }
}

#[derive(Debug)]
/// A tree of slices that can be sequentially iterated over.
pub enum SliceTree<'s, Item> {
    /// Leaf node - just contains a slice
    Slice(&'s [Item]),
    /// Branch node
    Branch {
        lhs: &'s SliceTree<'s, Item>,
        rhs: &'s SliceTree<'s, Item>,
        /// Total length of the inner nodes.
        ///
        /// This avoids a full tree traversal.
        total_length: usize,
    },
}

#[cfg(feature = "yoke")]
unsafe impl<'a, Item: 'static> yoke::Yokeable<'a> for SliceTree<'static, Item> {
    type Output = SliceTree<'a, Item>;

    #[inline]
    fn transform(&'a self) -> &'a Self::Output {
        self
    }
    #[inline]
    fn transform_owned(self) -> Self::Output {
        self
    }

    /// This is where the janky stuff starts ;p
    #[inline]
    unsafe fn make(from: Self::Output) -> Self {
        unsafe { core::mem::transmute_copy(&from) }
    }

    #[inline]
    fn transform_mut<F>(&'a mut self, f: F)
    where
        F: 'static + for<'b> FnOnce(&'b mut Self::Output),
    {
        // just taken from the yoke docs
        unsafe { f(core::mem::transmute::<&mut Self, &mut Self::Output>(self)) }
    }
}

// No need for conditioning on Item for copy
impl<'s, Item> Copy for SliceTree<'s, Item> {}
impl<'s, Item> Clone for SliceTree<'s, Item> {
    fn clone(&self) -> Self {
        match self {
            SliceTree::Slice(c) => SliceTree::Slice(c),
            SliceTree::Branch {
                lhs,
                rhs,
                total_length,
            } => SliceTree::Branch {
                lhs,
                rhs,
                total_length: *total_length,
            },
        }
    }
}

impl<'s, 'q: 's, Item> AsRef<SliceTree<'s, Item>> for SliceTree<'q, Item> {
    fn as_ref(&self) -> &SliceTree<'s, Item> {
        self
    }
}

impl<'s, Item> SliceTree<'s, Item> {
    /// Get the length of all the slices.
    ///
    /// Note that this relies on an accurate branch length or slice length.
    #[inline]
    pub fn len(&self) -> usize {
        match self {
            SliceTree::Slice(v) => v.len(),
            SliceTree::Branch {
                lhs: _,
                rhs: _,
                total_length,
            } => *total_length,
        }
    }

    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    #[inline]
    /// Build a slicetree from an initial slice
    pub fn from_slice(v: &'s [Item]) -> Self {
        Self::Slice(v)
    }

    #[inline]
    /// Build a slicetree from a pair of references
    pub fn from_branches<'q: 's>(
        (lhs, rhs): (&'s SliceTree<'q, Item>, &'s SliceTree<'q, Item>),
    ) -> Self {
        Self::Branch {
            lhs,
            rhs,
            total_length: lhs.len() + rhs.len(),
        }
    }

    /// Attempt to get the item at the given index.
    ///
    /// Returns None if the index is out of range.
    ///
    /// Note that if the total length values are wrong, this may return [`None`] even when
    /// the range is correct.
    ///
    /// In particular, if the range is larger than the actual range of values, this might happen
    /// (if it's smaller, then the indexing operations may actually skip elements).
    pub fn get(&self, idx: usize) -> Option<&'s Item> {
        let mut current_depth_branch = self;
        // If we go left on a branch then the index into that branch is the same
        // However if we go right then the index into the right branch is the index
        // into the whole thing minus the length of the right branch.
        let mut index_normalised_to_branch_length = idx;
        while let SliceTree::Branch {
            lhs,
            rhs,
            total_length,
        } = current_depth_branch
        {
            // If the index is >= to total length, it is too big
            if index_normalised_to_branch_length >= *total_length {
                return None;
            };

            if index_normalised_to_branch_length >= lhs.len() {
                // Go right
                index_normalised_to_branch_length -= lhs.len();
                current_depth_branch = rhs;
            } else {
                // Go left
                current_depth_branch = lhs;
            }
        }

        if let SliceTree::Slice(s) = current_depth_branch {
            s.get(index_normalised_to_branch_length)
        } else {
            unreachable!("We iterated all the branches.");
        }
    }
}

#[cfg(feature = "fuzzing")]
/// Aid for those writing fuzzers.
pub mod fuzzing {
    use core::iter::once;

    use alloc::{boxed::Box, collections::BTreeMap};
    use arbitrary::Arbitrary;
    use chain_trans::Trans;
    pub use typed_arena;
    use typed_arena::Arena;
    pub use yoke::Yoke;

    #[cfg(feature = "std")]
    #[cfg(feature = "alloc")]
    use alloc::vec::Vec;

    use crate::slicetree::IntoSliceTreeFromRefPair;

    pub use super::SliceTree;

    type SliceTreeOwningParts<Item> = Box<(Arena<SliceTree<'static, Item>>, Vec<Item>)>;

    /// Slice tree that builds itself from a raw input vector and information on how to divide it
    pub struct OwnedSliceTree<Item: 'static> {
        contents: Yoke<SliceTree<'static, Item>, SliceTreeOwningParts<Item>>,
    }

    impl<Item: 'static> OwnedSliceTree<Item> {
        /// Get a reference to the flat slice the tree was generated from (this slice should be in
        /// the same order as the tree contents)
        pub fn flat_slice(&self) -> &[Item] {
            self.contents.backing_cart().1.as_ref()
        }

        /// Get the slice tree as a reference
        pub fn slice_tree_ref(&self) -> &'_ SliceTree<'_, Item> {
            self.contents.get()
        }

        /// Get a by-value ref to the slice tree
        pub fn slice_tree_val(&self) -> SliceTree<'_, Item> {
            *self.slice_tree_ref()
        }
    }

    impl<Item: core::fmt::Debug> core::fmt::Debug for OwnedSliceTree<Item> {
        fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
            f.debug_struct("OwnedSliceTree")
                .field("flat_slice", &self.flat_slice())
                .field("slice_tree", &self.slice_tree_val())
                .finish_non_exhaustive()
        }
    }

    impl<'a, Item: Arbitrary<'a>> Arbitrary<'a> for OwnedSliceTree<Item> {
        fn arbitrary(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<Self> {
            let bare_vector = Vec::<Item>::arbitrary(u)?;
            let bare_arena = Arena::new();

            fn build_from_bare_slice_and_arena<'yokelt, 'a, Item>(
                (arena, bare_slice): &'yokelt (Arena<SliceTree<'static, Item>>, Vec<Item>),
                u: &mut arbitrary::Unstructured<'a>,
            ) -> Result<SliceTree<'yokelt, Item>, arbitrary::Error> {
                // Obtain split point count (must be < len of original)
                // This might be ZERO
                let split_points = u.choose_index(bare_slice.len())?;
                // Generate splitpoints,
                //
                // making sure to add 0 and len() at the end so we dont cut off ends by
                // accident
                //
                // Also the if splitpoints != 0 check is not actually strictly necessary
                // but adding it exercises the code paths for when we have no split points.
                //
                // Even with `0` at the start the windows(2) iterator will never use it if there
                // are no split points
                let split_locations: Vec<usize> = once(Ok(0))
                    .chain(
                        (0..split_points)
                            .into_iter()
                            .map(|_| u.choose_index(bare_slice.len()))
                            .chain(
                                if split_points != 0 {
                                    Some(Ok(bare_slice.len()))
                                } else {
                                    None
                                }
                                .iter()
                                .copied(),
                            ),
                    )
                    .collect::<Result<Vec<_>, _>>()?
                    // Sorting so that the chunk slices have ordered indices
                    .trans_mut(|collection: &mut Vec<_>| collection.sort_unstable());

                // TODO when array_windows is stabilised, use that
                //
                // Stores a stable index associated with each tree element - note that over time these
                // will be removed.
                let mut slicelist = split_locations
                    .windows(2)
                    .map(|startendpair| &bare_slice[startendpair[0]..startendpair[1]])
                    // 'static is actually lifetime of the yoke cart here
                    // so we do a little unsafe finangling :/
                    .map(|slce: &'yokelt [Item]| SliceTree::<'yokelt, Item>::Slice(slce))
                    .enumerate()
                    .collect::<BTreeMap<usize, SliceTree<'yokelt, Item>>>();

                // Merge all the trees, storing references in the arena
                //
                // This always keeps the order all fine, doesn't blow up the stack, and produces
                // randomly organised trees.
                //
                // This replaces some randomly chosen value at an index and at index +1, with the
                // merged version of those trees.
                while slicelist.len() >= 2 {
                    let merge_index = u.choose_index(slicelist.len() - 1)?;
                    // Take the nth key.
                    // Note that we can't, unfortunately, just index, because we are removing
                    // intermediary keys and hence the random value range does not correspond to
                    // the range of existing keys
                    //
                    // Ergo, we have to instead iterate over the keys until we reach enough, take two
                    // keys, and do things with those.
                    //
                    // While this is O(n), I think that constructing trees like this has some
                    // unavoidable O(n)-ness - either you have to reshuffle all the keys to keep track
                    // of which keys map to real indices, or you store stuff in a vec and have to
                    // constantly reshuffle the items around as you remove them, or any other number of
                    // things.
                    let mut iterator = slicelist.keys();
                    let lhs_key = *iterator
                        .by_ref()
                        .nth(merge_index)
                        .expect("merge index < len() - 1 so should be shorter than iter");
                    let rhs_key = *iterator
                        .next()
                        .expect("merge_index < len() - 1 so merge_index + 1 < len()");

                    // take the two trees and merge them.
                    let rhs = slicelist
                        .remove(&rhs_key)
                        .expect("just got key in iterator");
                    let lhs = slicelist
                        .remove(&lhs_key)
                        .expect("just got key in iterator");

                    // Safety:
                    // * We are yoking and holding references to the Arena
                    // * By definition of the Yoke, arena references are the same lifetime as that
                    //   the overall Yoke object, 'yokelt
                    // * However, we have to store 'static stuff in the arena because we can't
                    //   get yoke lts in it's static type signature.
                    let lhs_lt_erased: SliceTree<'static, Item> =
                        unsafe { core::mem::transmute_copy(&lhs) };
                    let rhs_lt_erased: SliceTree<'static, Item> =
                        unsafe { core::mem::transmute_copy(&rhs) };

                    slicelist.insert(
                        lhs_key,
                        (&*arena.alloc(lhs_lt_erased), &*arena.alloc(rhs_lt_erased))
                            .into_slice_tree(),
                    );
                }

                // If there was no split points initially the map will be empty. In that
                // case, use the raw vec slice directly as a singleton
                let maybe_last_key = slicelist.keys().next().cloned();
                let final_tree = if let Some(last_key) = maybe_last_key {
                    // The final tree:
                    slicelist
                        .remove(&last_key)
                        .expect("only one last tree too since we had a key")
                } else {
                    SliceTree::Slice(bare_slice)
                };
                Ok(final_tree)
            }

            Ok(Self {
                contents: Yoke::try_attach_to_cart(Box::new((bare_arena, bare_vector)), |v| {
                    build_from_bare_slice_and_arena(v, u)
                })?,
            })
        }

        fn size_hint(depth: usize) -> (usize, Option<usize>) {
            use arbitrary::size_hint::*;

            Vec::<Item>::size_hint(depth)
                .trans(|a| and(a, usize::size_hint(depth)))
                // Split points
                .trans(|a| and(a, (0, None)))
        }
    }
}

// core-alloc
// Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
