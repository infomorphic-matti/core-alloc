#![doc = include_str!("../README.md")]
#![no_std]
#![deny(unsafe_op_in_unsafe_fn)]
#![cfg_attr(doc, feature(doc_auto_cfg))]
#![cfg_attr(feature = "allocator_api", feature(allocator_api))]

#[cfg(feature = "alloc")]
extern crate alloc;

#[cfg(feature = "std")]
extern crate std;

use core::alloc::Layout;
use core::ptr::{self, NonNull};

/// Reexport the sptr (strict provenance) polyfill - see [the `sptr` repository here](https://github.com/Gankra/sptr)
pub use sptr;

pub mod instrument;
pub mod restrict;
pub mod slicetree;

/// Indicates an error in allocation.
///
/// In the case where the alloc API is actually available, this is that error instead.
#[cfg(not(feature = "allocator_api"))]
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct AllocErrorInner;

#[cfg(not(feature = "allocator_api"))]
impl core::fmt::Display for AllocErrorInner {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_str("memory allocation failed")
    }
}

#[cfg(feature = "allocator_api")]
type AllocErrorInner = core::alloc::AllocError;

/// Allocation error type.
///
/// Note the weird type aliasing is for documentation purposes - autodoc for features thinks this
/// is only available with the alloc api feature if it directly switches on
/// feature="allocator_api". Adding indirection fixes it.
pub type AllocError = AllocErrorInner;

/// Needed because type aliases can't be used as constructors, even when the aliased type is
/// zero-sized.
#[inline(always)]
#[cfg(feature = "allocator_api")]
const fn alloc_error_inner() -> AllocError {
    core::alloc::AllocError
}

/// Needed because type aliases can't be used as constructors, even when the aliased type is
/// zero-sized.
#[inline(always)]
#[cfg(not(feature = "allocator_api"))]
const fn alloc_error_inner() -> AllocError {
    AllocErrorInner
}

#[inline(always)]
/// Get an instance of the crate's allocator error type.
///
/// If using allocator API, this is an alias for that. If not, it is a simple unit type defined by
/// the crate.
pub const fn alloc_error() -> AllocError {
    alloc_error_inner()
}

/// like the unstable feature to get the first part of the slice pointer as a pointer to its
/// element.
#[inline(always)]
fn as_non_null_ptr<T>(slice_ptr: NonNull<[T]>) -> NonNull<T> {
    // uses the inner implementation in *mut [T]
    // SAFETY
    // we know the slice is nonnull
    unsafe { NonNull::new_unchecked(slice_ptr.as_ptr() as *mut T) }
}

/// Trait for an allocator that provides extended capabilities - a lot of this is the same as the
/// allocator API trait (with docs copied from there), but with one or two added capabilities.
///
/// An allocator of this kind is similar to allocator_api allocators. However, it has several
/// differences. In particular, it possesses an extra operation that allows it to cleanly split an
/// allocation into two peices.
///
/// Some of this documentation is taken from the alloc standard library crate.
///
/// ### Allocator Constraints
/// Extended Allocators are designed to be implemented on zero-sized types, references,
/// or smartpointers - or wrappers around those (especially such that we avoid blanket impls on
/// allocator-api allocators). If using something else this would cause issues with pointers being
/// invalidated by the thing containing the allocator moving around. (this issue could also be
/// accounted for by the [storage api allocator
/// alternative](https://rust-lang.zulipchat.com/#narrow/stream/197181-t-libs.2Fwg-allocators/topic/Storages.20via.20Layout))
///
/// Zero sized allocations are allowed in extended allocators, just like in allocator_api style
/// allocators. If an underlying system allocator doesn't support them, it must be caught by the
/// implementation.
///
/// ### Currently Allocated Memory
///
/// Some of the methods require that a memory block be *currently allocated* via an allocator. This
/// means that:
/// * The starting address for the memory block was previously returned by [`allocate`], [`allocate_zeroed`], [`grow`],
///   [`grow_zeroed`], [`shrink`], or [`split`]
/// * The memory block has not been subsequently deallocated, where blocks are either deallocated
///   directly by being passed to [`deallocate`] or were changed by being passed to [`grow`],
///   [`grow_zeroed`], [`shrink`] or [`split`] that returned [`Ok`]. If [`grow`], [`grow_zeroed`],
///   [`shrink`] or [`split`] have returned [`Err`], the block originally passed to them remains
///   valid.
///
/// [`allocate`]: ExtendedAllocator::allocate
/// [`allocate_zeroed`]: ExtendedAllocator::allocate_zeroed
/// [`grow`]: ExtendedAllocator::grow
/// [`grow_zeroed`]: ExtendedAllocator::grow_zeroed
/// [`shrink`]: ExtendedAllocator::shrink
/// [`split`]: ExtendedAllocator::split
/// [`deallocate`]: ExtendedAllocator::deallocate
///
/// ### Memory Fitting
///
/// Some of the methods require that a layout *fit* a memory block. What it means for a layout to
/// "fit" a memory block means (or equivalently, for a memory block to "fit" a layout) is that the
/// following conditions must hold:
///
/// * The block must be allocated with the same alignment as [`layout.align()`], and
///
/// * The provided [`layout.size()`] must fall in the range `min ..= max`, where:
///   - `min` is the size of the layout most recently used to allocate the block, and
///   - `max` is the latest actual size returned from [`allocate`], [`grow`], [`grow_zeroed`], [`shrink`], or [`split`].
///
/// [`layout.align()`]: Layout::align
/// [`layout.size()`]: Layout::size
///
/// Conceptually, this means that when growing/shrinking/deallocating/etc. that the allocator must
/// account for any extra padding memory it provided to the user. With [`split`], the arguments for
/// new start and end layouts correspond directly to the two output regions produced.
///
/// # Note on Multithreaded
/// Allocators are allowed to be Sync *or* local thread only.
///
/// # Safety
///
/// * Memory blocks returned from an allocator must point to valid memory and retain their validity
///   until the instance and all of its clones are dropped. In particular, the memory handed out
///   must be valid for the lifetime of the allocator.
///
/// * Cloning or moving the allocator must not invalidate memory blocks returned from this
///   allocator. A cloned allocator must behave like the same allocator.
///
/// * Any pointer to a memory block which is [*currently allocated*] may be passed to any other
///   method of the allocator.
///
/// [*currently allocated*]: #currently-allocated-memory
pub unsafe trait ExtendedAllocator {
    /// Attempts to allocate a block of memory.
    ///
    /// On success, returns a [`NonNull<[u8]>`][NonNull] meeting the size and alignment guarantees of `layout`.
    ///
    /// The returned block may have a larger size than specified by `layout.size()`, and may or may
    /// not have its contents initialized.
    ///
    /// # Errors
    ///
    /// Returning `Err` indicates that either memory is exhausted or `layout` does not meet
    /// allocator's size or alignment constraints.
    ///
    /// Implementations are encouraged to return `Err` on memory exhaustion rather than panicking or
    /// aborting, but this is not a strict requirement. (Specifically: it is *legal* to implement
    /// this trait atop an underlying native allocation library that aborts on memory exhaustion.)
    ///
    /// Clients wishing to abort computation in response to an allocation error are encouraged to
    /// call the [`handle_alloc_error`] function, rather than directly invoking `panic!` or similar.
    ///
    /// [`handle_alloc_error`]: alloc::alloc::handle_alloc_error
    fn allocate(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError>;

    /// Behaves like `allocate`, but also ensures that the returned memory is zero-initialized.
    ///
    /// # Errors
    ///
    /// Returning `Err` indicates that either memory is exhausted or `layout` does not meet
    /// allocator's size or alignment constraints.
    ///
    /// Implementations are encouraged to return `Err` on memory exhaustion rather than panicking or
    /// aborting, but this is not a strict requirement. (Specifically: it is *legal* to implement
    /// this trait atop an underlying native allocation library that aborts on memory exhaustion.)
    ///
    /// Clients wishing to abort computation in response to an allocation error are encouraged to
    /// call the [`handle_alloc_error`] function, rather than directly invoking `panic!` or similar.
    ///
    /// [`handle_alloc_error`]: alloc::alloc::handle_alloc_error
    fn allocate_zeroed(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
        let ptr = self.allocate(layout)?;
        // SAFETY: `alloc` returns a valid memory block
        // NOTE that the `as_non_null_ptr` is unstable... we do not want instability.
        //
        // so we reimplement the function ourselves.
        // ptr.len() gets # of elements in the slice, 0 fills up each inner T slice element
        // ptr.len() times.
        unsafe { as_non_null_ptr(ptr).as_ptr().write_bytes(0, ptr.len()) }
        Ok(ptr)
    }

    /// Deallocates the memory referenced by `ptr`.
    ///
    /// # Safety
    ///
    /// * `ptr` must denote a block of memory [*currently allocated*] via this allocator, and
    /// * `layout` must [*fit*] that block of memory.
    ///
    /// [*currently allocated*]: #currently-allocated-memory
    /// [*fit*]: #memory-fitting
    unsafe fn deallocate(&self, ptr: NonNull<u8>, layout: Layout);

    /// Attempts to extend the memory block.
    ///
    /// Returns a new [`NonNull<[u8]>`][NonNull] containing a pointer and the actual size of the allocated
    /// memory. The pointer is suitable for holding data described by `new_layout`. To accomplish
    /// this, the allocator may extend the allocation referenced by `ptr` to fit the new layout.
    ///
    /// If this returns `Ok`, then ownership of the memory block referenced by `ptr` has been
    /// transferred to this allocator. The memory may or may not have been freed, and should be
    /// considered unusable.
    ///
    /// If this method returns `Err`, then ownership of the memory block has not been transferred to
    /// this allocator, and the contents of the memory block are unaltered.
    ///
    /// # Safety
    ///
    /// * `ptr` must denote a block of memory [*currently allocated*] via this allocator.
    /// * `old_layout` must [*fit*] that block of memory (The `new_layout` argument need not fit it.).
    /// * `new_layout.size()` must be greater than or equal to `old_layout.size()`.
    ///
    /// Note that `new_layout.align()` need not be the same as `old_layout.align()`.
    ///
    /// [*currently allocated*]: #currently-allocated-memory
    /// [*fit*]: #memory-fitting
    ///
    /// # Errors
    ///
    /// Returns `Err` if the new layout does not meet the allocator's size and alignment
    /// constraints of the allocator, or if growing otherwise fails.
    ///
    /// Implementations are encouraged to return `Err` on memory exhaustion rather than panicking or
    /// aborting, but this is not a strict requirement. (Specifically: it is *legal* to implement
    /// this trait atop an underlying native allocation library that aborts on memory exhaustion.)
    ///
    /// Clients wishing to abort computation in response to an allocation error are encouraged to
    /// call the [`handle_alloc_error`] function, rather than directly invoking `panic!` or similar.
    ///
    /// [`handle_alloc_error`]: alloc::alloc::handle_alloc_error
    unsafe fn grow(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        debug_assert!(
            new_layout.size() >= old_layout.size(),
            "`new_layout.size()` must be greater than or equal to `old_layout.size()`"
        );

        let new_ptr = self.allocate(new_layout)?;

        // SAFETY: because `new_layout.size()` must be greater than or equal to
        // `old_layout.size()`, both the old and new memory allocation are valid for reads and
        // writes for `old_layout.size()` bytes. Also, because the old allocation wasn't yet
        // deallocated, it cannot overlap `new_ptr`. Thus, the call to `copy_nonoverlapping` is
        // safe. The safety contract for `dealloc` must be upheld by the caller.
        unsafe {
            ptr::copy_nonoverlapping(
                ptr.as_ptr(),
                as_non_null_ptr(new_ptr).as_ptr(),
                old_layout.size(),
            );
            self.deallocate(ptr, old_layout);
        }

        Ok(new_ptr)
    }

    /// Behaves like `grow`, but also ensures that the new contents are set to zero before being
    /// returned.
    ///
    /// The memory block will contain the following contents after a successful call to
    /// `grow_zeroed`:
    ///   * Bytes `0..old_layout.size()` are preserved from the original allocation.
    ///   * Bytes `old_layout.size()..old_size` will either be preserved or zeroed, depending on
    ///     the allocator implementation. `old_size` refers to the size of the memory block prior
    ///     to the `grow_zeroed` call, which may be larger than the size that was originally
    ///     requested when it was allocated.
    ///   * Bytes `old_size..new_size` are zeroed. `new_size` refers to the size of the memory
    ///     block returned by the `grow_zeroed` call.
    ///
    /// # Safety
    ///
    /// * `ptr` must denote a block of memory [*currently allocated*] via this allocator.
    /// * `old_layout` must [*fit*] that block of memory (The `new_layout` argument need not fit it.).
    /// * `new_layout.size()` must be greater than or equal to `old_layout.size()`.
    ///
    /// Note that `new_layout.align()` need not be the same as `old_layout.align()`.
    ///
    /// [*currently allocated*]: #currently-allocated-memory
    /// [*fit*]: #memory-fitting
    ///
    /// # Errors
    ///
    /// Returns `Err` if the new layout does not meet the allocator's size and alignment
    /// constraints of the allocator, or if growing otherwise fails.
    ///
    /// Implementations are encouraged to return `Err` on memory exhaustion rather than panicking or
    /// aborting, but this is not a strict requirement. (Specifically: it is *legal* to implement
    /// this trait atop an underlying native allocation library that aborts on memory exhaustion.)
    ///
    /// Clients wishing to abort computation in response to an allocation error are encouraged to
    /// call the [`handle_alloc_error`] function, rather than directly invoking `panic!` or similar.
    ///
    /// [`handle_alloc_error`]: alloc::alloc::handle_alloc_error
    unsafe fn grow_zeroed(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        debug_assert!(
            new_layout.size() >= old_layout.size(),
            "`new_layout.size()` must be greater than or equal to `old_layout.size()`"
        );

        let new_ptr = self.allocate_zeroed(new_layout)?;

        // SAFETY: because `new_layout.size()` must be greater than or equal to
        // `old_layout.size()`, both the old and new memory allocation are valid for reads and
        // writes for `old_layout.size()` bytes. Also, because the old allocation wasn't yet
        // deallocated, it cannot overlap `new_ptr`. Thus, the call to `copy_nonoverlapping` is
        // safe. The safety contract for `dealloc` must be upheld by the caller.
        unsafe {
            ptr::copy_nonoverlapping(
                ptr.as_ptr(),
                as_non_null_ptr(new_ptr).as_ptr(),
                old_layout.size(),
            );
            self.deallocate(ptr, old_layout);
        }

        Ok(new_ptr)
    }

    /// Attempts to shrink the memory block.
    ///
    /// Returns a new [`NonNull<[u8]>`][NonNull] containing a pointer and the actual size of the allocated
    /// memory. The pointer is suitable for holding data described by `new_layout`. To accomplish
    /// this, the allocator may shrink the allocation referenced by `ptr` to fit the new layout.
    ///
    /// If this returns `Ok`, then ownership of the memory block referenced by `ptr` has been
    /// transferred to this allocator. The memory may or may not have been freed, and should be
    /// considered unusable.
    ///
    /// If this method returns `Err`, then ownership of the memory block has not been transferred to
    /// this allocator, and the contents of the memory block are unaltered.
    ///
    /// # Safety
    ///
    /// * `ptr` must denote a block of memory [*currently allocated*] via this allocator.
    /// * `old_layout` must [*fit*] that block of memory (The `new_layout` argument need not fit it.).
    /// * `new_layout.size()` must be smaller than or equal to `old_layout.size()`.
    ///
    /// Note that `new_layout.align()` need not be the same as `old_layout.align()`.
    ///
    /// [*currently allocated*]: #currently-allocated-memory
    /// [*fit*]: #memory-fitting
    ///
    /// # Errors
    ///
    /// Returns `Err` if the new layout does not meet the allocator's size and alignment
    /// constraints of the allocator, or if shrinking otherwise fails.
    ///
    /// Implementations are encouraged to return `Err` on memory exhaustion rather than panicking or
    /// aborting, but this is not a strict requirement. (Specifically: it is *legal* to implement
    /// this trait atop an underlying native allocation library that aborts on memory exhaustion.)
    ///
    /// Clients wishing to abort computation in response to an allocation error are encouraged to
    /// call the [`handle_alloc_error`] function, rather than directly invoking `panic!` or similar.
    ///
    /// [`handle_alloc_error`]: alloc::alloc::handle_alloc_error
    unsafe fn shrink(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        debug_assert!(
            new_layout.size() <= old_layout.size(),
            "`new_layout.size()` must be smaller than or equal to `old_layout.size()`"
        );

        let new_ptr = self.allocate(new_layout)?;

        // SAFETY: because `new_layout.size()` must be lower than or equal to
        // `old_layout.size()`, both the old and new memory allocation are valid for reads and
        // writes for `new_layout.size()` bytes. Also, because the old allocation wasn't yet
        // deallocated, it cannot overlap `new_ptr`. Thus, the call to `copy_nonoverlapping` is
        // safe. The safety contract for `dealloc` must be upheld by the caller.
        unsafe {
            ptr::copy_nonoverlapping(
                ptr.as_ptr(),
                as_non_null_ptr(new_ptr).as_ptr(),
                new_layout.size(),
            );
            self.deallocate(ptr, old_layout);
        }

        Ok(new_ptr)
    }

    /// Creates a "by reference" adapter for this instance of `Allocator`.
    ///
    /// The returned adapter also implements `Allocator` and will simply borrow this.
    #[inline(always)]
    fn by_ref(&self) -> &Self
    where
        Self: Sized,
    {
        self
    }

    /// Attempt to split an allocation into two allocations. This is... pretty complex.
    /// The passed old pointer has to *fit* the old layout. This means that it is laid out
    /// something like the following:
    /// ```text
    ///  addO [OOOO OOOO OOOO OOOO OOOO OOOO pppp pppp]
    ///       |         |         |         |         |
    ///       c                   c                   c
    /// ```
    ///  where | represents alignment boundaries of the old layout, O is object memory, and p is
    ///  some padding provided by the allocator. The provided original layout must *fit* this
    ///  memory block - in particular, it's size can be anywhere from #Os to #(Os + Ps). addO is
    ///  the old address.
    ///  
    ///  In this case, the original allocation had:
    ///   * `layout.size() == 24` (24 byte size)
    ///   * `layout.align() == 8` (8 byte alignment)
    ///   * The allocator provided a 32 byte chunk of memory as output - for our example, we
    ///   suppose our allocator provides memory in 16 byte chunks, marked with `c`
    ///
    /// This can be split hypothetically at any byte in or one-after the Os indicated by the layout.
    /// When splitting an allocation, the new layouts can have smaller or larger alignments, both than
    /// the original and each other. This function must return chunks that, when provided with each layout,
    /// can be deallocated *separately* from each other.
    ///
    /// The starting new layout must receive all `start_layout.size()` bytes from the original
    /// allocation in it's chunk, aligned to `start_layout.align()`. The ending new layout must
    /// receive the first `end_layout.size()` bytes after the `start_layout.size()` bytes, with
    /// `start_layout.size() + end_layout.size() <= old_layout.size()`
    ///
    /// # Safety
    /// For this function to work, the following criteria must be met:
    /// * `old_layout` must [*fit*] the passed in block of memory.
    /// * `old_allocation` must represent a [*currently allocated*] memory block in this allocator.
    /// * `new_layout_start.size() + new_layout_end.size()` must be less than or equal to
    /// `old_layout.size()`.
    ///
    /// The alignments of the new layouts do not need to match the alignment of the old layout.
    ///
    /// This function will produce two separate allocations as a result. These can each be
    /// deallocated separately.
    ///
    /// An allocator can use this function to allow user code to avoid excess allocation. In
    /// particular:
    /// * In the case that the two new layouts have weaker alignment requirements than the old
    /// layout, the allocator may be able to either avoid new memory usage for a copy entirely, or
    /// only need to perform minimal extension of the latter part of it's allocation and shift the
    /// end layout along by a small number of bytes, to meet chunking requirements of the
    /// allocator.
    /// * In the case that the two new layouts have stronger alignment requirements, certain types
    /// of allocator can still likely get away with just shuffling the bytes along a little bit.
    ///
    /// This function must be atomic on failure. That is, if it fails, the original block of memory
    /// must still be valid. If it does not fail, then the original block of memory is invalidated
    /// and should not be accessed again.
    ///
    /// # Default implementation.
    /// The default implementation - due to lack of knowledge of how the inner allocator marks
    /// memory as allocated - must just allocate a new chunk for the second half.
    ///
    /// [*currently allocated*]: #currently-allocated-memory
    /// [*fit*]: #memory-fitting
    unsafe fn split(
        &self,
        old_allocation: NonNull<u8>,
        old_layout: Layout,
        new_layout_start: Layout,
        new_layout_end: Layout,
    ) -> Result<(NonNull<[u8]>, NonNull<[u8]>), AllocError> {
        debug_assert!(
            new_layout_start.size() + new_layout_end.size() <= old_layout.size(),
            "`new_layout_start.size() + new_layout_end.size()` must be less than or equal to `old_layout.size()`"
        );
        // Default implementations allocate for the second half of the layout, copy from the old
        // layout, and shrink the old allocation to match the front layout.
        //
        // This means that the only part of the allocation that can damage the input is last, so an
        // intermediary process succeeding does not wreck the original memory location in case a
        // latter part fails, preserving atomicity.
        let new_second_half = self.allocate(new_layout_end)?;

        // Copy memory - This is safe, because the two new layouts must be <= the old layout
        // length, so they are always good to read.
        //
        // The old memory and new memory can't overlap either because the old memory has not been
        // shrunk or otherwise deallocated yet.

        unsafe {
            // get the second chunk and copy over :)
            ptr::copy_nonoverlapping(
                old_allocation.as_ptr().add(new_layout_start.size()),
                as_non_null_ptr(new_second_half).as_ptr(),
                new_layout_end.size(),
            );
        }
        // Shrink the original
        // SAFETY:
        // * new_layout_start.size() + new_layout_end.size() <= old_layout.size()
        //   => new_layout_start.size() <= old_layout.size()
        // * Old layout must fit in the original block, as defined by the function contract
        // * old_allocation must denote a block produced by this allocator, as defined in the
        // function safety contract.
        unsafe { self.shrink(old_allocation, old_layout, new_layout_start) }
            .map(|new_first_half| (new_first_half, new_second_half))
    }
}

unsafe impl<A> ExtendedAllocator for &A
where
    A: ExtendedAllocator + ?Sized,
{
    #[inline]
    fn allocate(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
        (**self).allocate(layout)
    }

    #[inline]
    fn allocate_zeroed(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
        (**self).allocate_zeroed(layout)
    }

    #[inline]
    unsafe fn deallocate(&self, ptr: NonNull<u8>, layout: Layout) {
        // SAFETY: the safety contract must be upheld by the caller
        unsafe { (**self).deallocate(ptr, layout) }
    }

    #[inline]
    unsafe fn grow(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        // SAFETY: the safety contract must be upheld by the caller
        unsafe { (**self).grow(ptr, old_layout, new_layout) }
    }

    #[inline]
    unsafe fn grow_zeroed(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        // SAFETY: the safety contract must be upheld by the caller
        unsafe { (**self).grow_zeroed(ptr, old_layout, new_layout) }
    }

    #[inline]
    unsafe fn shrink(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        // SAFETY: the safety contract must be upheld by the caller
        unsafe { (**self).shrink(ptr, old_layout, new_layout) }
    }

    #[inline]
    unsafe fn split(
        &self,
        old_allocation: NonNull<u8>,
        old_layout: Layout,
        new_layout_start: Layout,
        new_layout_end: Layout,
    ) -> Result<(NonNull<[u8]>, NonNull<[u8]>), AllocError> {
        // SAFETY: the safety contract must be upheld by the caller.
        unsafe { (**self).split(old_allocation, old_layout, new_layout_start, new_layout_end) }
    }
}

#[cfg(feature = "allocator_api")]
#[derive(Clone, Copy, Debug)]
/// Use an allocator-api allocator as an [`ExtendedAllocator`]
///
/// This is a newtype rather than a generic trait impl so that implementors of
/// [`core::alloc::Allocator`] can implement the extra features in [`ExtendedAllocator`] at
/// lesiure when available.
///
/// This will use only the APIs provided by [`core::alloc::Allocator`], which may leave splitting
/// efficiency on the table if this library forced authors to implement only one of `Allocator` or
/// `ExtendedAllocator` due to auto-implementations
pub struct Alloc<A: core::alloc::Allocator + ?Sized>(pub A);

/// Implement an [`ExtendedAllocator`] using only the APIs in [`core::alloc::Allocator`].
///
/// This will lose out on potential higher efficiency for splitting allocations if your allocator
/// provides it, but it will work for any standard allocator if needed.
#[cfg(feature = "allocator_api")]
#[allow(unsafe_op_in_unsafe_fn)]
unsafe impl<A: core::alloc::Allocator + ?Sized> ExtendedAllocator for Alloc<A> {
    #[inline(always)]
    fn allocate(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
        self.0.allocate(layout)
    }

    #[inline(always)]
    unsafe fn deallocate(&self, ptr: NonNull<u8>, layout: Layout) {
        self.0.deallocate(ptr, layout)
    }

    #[inline(always)]
    fn allocate_zeroed(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
        self.0.allocate_zeroed(layout)
    }

    #[inline(always)]
    unsafe fn grow(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        self.0.grow(ptr, old_layout, new_layout)
    }

    #[inline(always)]
    unsafe fn grow_zeroed(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        self.0.grow_zeroed(ptr, old_layout, new_layout)
    }

    #[inline(always)]
    unsafe fn shrink(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        self.0.shrink(ptr, old_layout, new_layout)
    }
}

#[cfg(feature = "fuzzing")]
/// Utilities to fuzz extended allocators correctly.
///
/// This contains tools to generate an arbitrary sequence of allocation events and prevent
/// segfaults or similar.
///
/// # Modelling Arbitrary Allocations
/// To test your allocator, you need to model a sequence of valid allocation and deallocation
/// operations. In particular, you can't use an already freed chunk of memory, and when randomising
/// order you can't reorder any operations before the creation of their memory chunks or after the
/// deallocation of their memory chunks.
///
/// Accurately modelling memory is most simply done if we consider each individual allocation as a
/// node in a directed tree of memory operations, where each operation is dependent-upon the memory
/// block produced by some previous operation (if any). As each memory operation either invalidates
/// the passed in memory block, or fails and leaves it untouched, we can randomize all the
/// operations as long as we sort it such that each operation depending on another happens after
/// it.
///
/// Building this kind of chain can also be done "live", if we keep a list of currently
/// floating/modifiable memory blocks in existence and remove them when they are used.
///
/// # Deallocation
/// Rust programs are actually allowed to leak memory in a controlled manner. While testing this
/// usecase would theoretically be useful, in practice it causes problems - in particular, when
/// dealing with allocators that wrap, say, a global allocator.
///
/// When running long-running tests, if you delegate to a backing allocator and then leak memory,
/// memory will likely leak across test runs rather than just within a single one. By default, a
/// collection of allocations is "completed" - that is, any un-deallocated memory blocks that
/// aren't already used in another operation have a deallocation appended to their chain of
/// operations. This can be disabled.
///
/// # Memory Fitting
/// Rust allocators require - when consuming a memory block for an operation - that a layout is
/// provided. However, the layout does not have to be identical to that used when allocating a
/// memory block. It merely needs to *fit* the memory block - that is, it's size must be between
/// the original size specified in the requested layout, and the actual size returned as an
/// allocation.
///
/// A potential source of allocator bugs is that they don't work with layouts that aren't identical
/// to that used for initial allocation. As such, the fuzzing tools here automatically add a degree
/// of deviation from the initial allocation size that is used to modify the layout such that the
/// memory block merely fits it rather than it being the initial exact requested layout.
///
/// # Instrumented Allocators
/// Several types of allocators can be instrumented such that their true memory usage can be
/// accurately predicted ahead-of-time, via implementing [`crate::instrument::InstrumentedAllocator`].
/// For tools to fuzz such predictions for correctness, look at [`crate::instrument::fuzzing`].
pub mod fuzzing {
    // For strict pointer provenance
    #![allow(unstable_name_collisions)]
    use core::{
        alloc::{Layout, LayoutError},
        fmt::Display,
        mem::size_of,
        num::NonZeroUsize,
        ptr::NonNull,
    };
    use std::{collections::HashMap, thread_local};

    use alloc::vec::Vec;
    use arbitrary::{Arbitrary, Error, Unstructured};
    use chain_trans::Trans;

    use crate::{AllocError, ExtendedAllocator};
    use sptr::Strict;

    #[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Debug)]
    /// The next available ID for a new memory block to take.
    pub(crate) struct NextIDCtr(usize);

    impl NextIDCtr {
        /// Get the next available ID.
        pub fn next(&mut self) -> ID {
            let r = ID(self.0);
            self.0 += 1;
            r
        }
    }

    #[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
    /// Identifier for a memory block in the sequence of allocation operations.
    pub struct ID(usize);

    impl Display for ID {
        fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
            f.write_str("ID{")?;
            self.0.fmt(f)?;
            f.write_str("}")
        }
    }

    /// Single memory block allocation made in the operation. This documents the layout used for
    /// such. A new ID must be used for every independent memory block output from some allocator
    /// operation.
    #[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
    pub(crate) struct MemoryBlock {
        /// The layout used to construct this block.
        pub layout: Layout,
        /// Convenient unique identifier for the block
        pub id: ID,
    }

    pub(crate) enum SourceOrCreateMemblk {
        /// Consume some existing memory block in the operation. Note that the creation of this
        /// should *remove* the memory block from the availability list.
        Consume(MemoryBlock),
        /// Create a new memory block (allocate). This adds to the list of available memory
        /// blocks.
        ///
        /// This is in essence always an allocate operation.
        ///
        /// Bool indicates if zeroed or not.
        Create(MemoryBlock, bool),
    }

    /// Randomly selectable/arbitrary descriptor of allocator operations that consume a memory
    /// block.
    ///
    /// Has a maximum fitness deviation to enable non-explosive pessimistic size calculations. For
    /// an explanation of why this is important, see [`AllocatorEventSequence`] - it is inclusive.
    #[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
    pub(crate) enum ConsumingAllocatorOperationDescriptor<const MAXIMUM_FITNESS_DEVIATION: usize> {
        Deallocate {
            fit_deviation: NonZeroUsize,
        },
        Grow {
            fit_deviation: NonZeroUsize,
            zeroed: bool,
        },
        Shrink {
            fit_deviation: NonZeroUsize,
        },
        Split {
            fit_deviation: NonZeroUsize,
        },
    }

    impl<'a, const MAXIMUM_FITNESS_DEVIATION: usize> Arbitrary<'a>
        for ConsumingAllocatorOperationDescriptor<MAXIMUM_FITNESS_DEVIATION>
    {
        fn arbitrary(u: &mut Unstructured<'a>) -> arbitrary::Result<Self> {
            #[derive(Clone, Copy, Arbitrary, Debug)]
            enum JustTheOp {
                Deallocate,
                // zeroed or not
                Grow(bool),
                Shrink,
                Split,
            }

            let op: JustTheOp = u.arbitrary()?;
            // Conceptually, the fitness deviation is an index into memory. As such, we tell the
            // unstructured data as such.
            //
            // Note that fitness deviation is related to
            let fit_deviation = u
                .choose_index(MAXIMUM_FITNESS_DEVIATION)?
                .trans(|a| a + 1)
                .trans(NonZeroUsize::new)
                .expect("Just added 1");

            match op {
                JustTheOp::Deallocate => Self::Deallocate { fit_deviation },
                JustTheOp::Grow(zeroed) => Self::Grow {
                    fit_deviation,
                    zeroed,
                },
                JustTheOp::Shrink => Self::Shrink { fit_deviation },
                JustTheOp::Split => Self::Split { fit_deviation },
            }
            .trans(Ok)
        }
    }

    /// Represents - in the current generation cycle of an allocation operation - the available
    /// allocated memory blocks at the time, along with the id counter.
    #[derive(Default)]
    pub(crate) struct AvailableMemoryBlocks(pub HashMap<ID, MemoryBlock>, pub NextIDCtr);

    impl AvailableMemoryBlocks {
        /// Simply creates a new memory block from the given layout, inserting it into the list of
        /// available blocks.
        fn new_memory_block(&mut self, block_layout: Layout) -> MemoryBlock {
            let new_id = self.1.next();
            let new_memblk = MemoryBlock {
                layout: block_layout,
                id: new_id,
            };
            self.0.insert(new_id, new_memblk).trans(|v| {
                if v.is_some() {
                    panic!(
                        "Attempt to create new memory block but {} was duplicate",
                        new_id
                    )
                }
            });
            new_memblk
        }

        /// Creates a new memory block randomly and inserts it.
        ///
        /// Allows for providing a maximum length to the layout, optionally. Note that this maximum
        /// length is *inclusive*.
        ///
        /// Alignment is taken as a power of 2 randomly from 1 to 2^18
        fn generate_new_memory_block<'a>(
            &mut self,
            u: &mut Unstructured<'a>,
            max_layout_length: Option<usize>,
        ) -> Result<MemoryBlock, Error> {
            let actual_length = match max_layout_length {
                Some(limit) => u.int_in_range(0..=limit)?,
                // Will allocate at least some # of bytes as a contiguous chunk, so it is
                // conceptually a length even if we aren't using it directly to build a vec or
                // something like that
                None => u.arbitrary_len::<u8>()?,
            };

            let alignment = arbitrary_alignment(u)?;

            let memblock_layout = Layout::from_size_align(actual_length, alignment)
                .expect("Directly constructed power of two");

            Ok(self.new_memory_block(memblock_layout))
        }

        /// This either:
        /// * Picks an existing memory block for consumption in an operation - removing it from the
        ///   list of floating memory blocks
        /// * Creates a new memory block for an allocation operation, adding it to the list of
        ///  "allocated" memory blocks.
        fn pick_existing_or_create_new_memory_block<'a>(
            &mut self,
            u: &mut Unstructured<'a>,
        ) -> Result<SourceOrCreateMemblk, Error> {
            if self.0.keys().next().is_some() {
                // Can either consume an allocation or create one.
                let consume = u.ratio(1, 2)?;
                if consume {
                    // We don't need to actually collect the keys in memory, only iterate over them
                    // with .take() some number of times on the iterator.
                    let key_idx = u.int_in_range(0..=self.0.len() - 1)?;
                    let actual_key = *self
                        .0
                        .keys()
                        .nth(key_idx)
                        .expect("Selected index from length of keys");
                    Ok(SourceOrCreateMemblk::Consume(
                        self.0.remove(&actual_key).expect("Just took from key iter"),
                    ))
                } else {
                    let zeroed: bool = u.arbitrary()?;
                    self.generate_new_memory_block(u, None)?
                        .trans(|v| SourceOrCreateMemblk::Create(v, zeroed))
                        .trans(Ok)
                }
            } else {
                let zeroed: bool = u.arbitrary()?;
                self.generate_new_memory_block(u, None)?
                    .trans(|v| SourceOrCreateMemblk::Create(v, zeroed))
                    .trans(Ok)
            }
        }

        /// Generates an arbitrary operation that either consumes an existing memory block or
        /// produces a new one.
        ///
        /// Requires a compiletime maximum fitness deviation to allow calculating pessimistic input
        /// memory data sizes for ouput memory blocks to have valid layout size constraints. See
        /// [`AllocatorEventSequence`] for what this does in detail.
        ///
        /// Note that when the randomly picked fitness deviation is zero - because 0 % fitdev == 0 ∀fitdev -
        /// it can use the initial layout.
        ///
        /// second type parameter controls whether or not zeroing operations are generated.
        pub fn new_operation<
            'a,
            const MAXIMUM_FITNESS_DEVIATION: usize,
            const ZEROING_OPS: bool,
        >(
            &mut self,
            u: &mut Unstructured<'a>,
        ) -> Result<AllocatorOperation, Error> {
            /// Get a layout with the pessimistically maximum size that could potentially be added
            /// to it when the given fitting deviation is provided - essentially, the value of the
            /// layout with the given fit deviation *if* the allocated block was `isize::MAX` larger
            /// than the requested allocation.
            ///
            /// If the fit deviation is 1, though, then we know that the initial layout must
            /// just be sent back in because <random index> % 1 == 0
            const fn with_max_fit_deviation(
                v: Layout,
                fit_deviation: NonZeroUsize,
            ) -> Result<Layout, LayoutError> {
                match fit_deviation.get() - 1 {
                    0 => Ok(v),
                    max_len => Layout::from_size_align(v.size() + max_len, v.align()),
                }
            }

            let consume_or_make = self.pick_existing_or_create_new_memory_block(u)?;

            match consume_or_make {
                SourceOrCreateMemblk::Consume(consumed_memblk) => {
                    // Now we need to pick the type of operation and fitness deviation
                    let basic_type: ConsumingAllocatorOperationDescriptor<
                        MAXIMUM_FITNESS_DEVIATION,
                    > = u.arbitrary()?;
                    match basic_type {
                        ConsumingAllocatorOperationDescriptor::Deallocate { fit_deviation } => {
                            Ok(AllocatorOperation::Deallocate {
                                consumed_memblk,
                                fit_deviation,
                            })
                        }
                        ConsumingAllocatorOperationDescriptor::Grow {
                            fit_deviation,
                            zeroed,
                        } => {
                            // Note here that without bounding the fitness deviation, the minimum
                            // requested extension may grow *arbitrarily large*, because the
                            // fitness deviation may also grow arbitrarily large.
                            let maximum_input_layout_size =
                                with_max_fit_deviation(consumed_memblk.layout, fit_deviation)
                                    .expect("should be fine");
                            // Then, read in an *extension amount* and new align. Conceptually,
                            // memory allocations are collections of bytes, so we extract a length
                            // from the input
                            let extension: usize = u.arbitrary_len::<u8>()?;
                            let alignment = arbitrary_alignment(u)?;
                            let grown_layout = maximum_input_layout_size
                                .align_to(alignment)
                                .expect("no ridiculous alignment can be produced")
                                .trans(|s| {
                                    let base_size = s.size();
                                    let align = s.align();
                                    Layout::from_size_align(base_size + extension, align)
                                })
                                .expect(
                                    "Size shouldn't be so ridiculous that it overflows isize max",
                                );
                            Ok(AllocatorOperation::Grow {
                                consumed_memblk,
                                fit_deviation,
                                produced_memblk: self.new_memory_block(grown_layout),
                                zeroed: ZEROING_OPS && zeroed,
                            })
                        }
                        ConsumingAllocatorOperationDescriptor::Shrink { fit_deviation } => {
                            // Note that pessimism in this case just involves using the initial
                            // requested block size, as shrinking only requires that the new layout is <= in size to the old one.
                            //
                            // This includes shrinking operations where the new layout is the same
                            // as the old. Therefore this is conceptually an index into the old
                            // layout, but with one more above for the purposes of getting the last
                            // chunks.
                            let shrunk_size = u.choose_index(consumed_memblk.layout.size() + 1)?;
                            let new_arbitrary_align = arbitrary_alignment(u)?;
                            Layout::from_size_align(shrunk_size, new_arbitrary_align)
                                .expect("No weird aligns")
                                .trans(|a| self.new_memory_block(a))
                                .trans(|produced_memblk| AllocatorOperation::Shrink {
                                    consumed_memblk,
                                    fit_deviation,
                                    produced_memblk,
                                })
                                .trans(Ok)
                        }
                        ConsumingAllocatorOperationDescriptor::Split { fit_deviation } => {
                            // This is one of the most complex sets of requirements.
                            //
                            // In particular, the input size must be >= the sum of the output
                            // sizes. This means that with the current strategy of pessimistically
                            // understanding memory, it is not possible to test the code paths
                            // where:
                            // * the input uses a larger fitting layout to the memory block
                            // * AND the inputs can use chunks of that larger layout all fine.
                            // In particular because of the way the sizes of the output memory
                            // blocks are generated - that is, by partitioning the *pessimistic*
                            // input size - the actual operation can only request up to the minimum
                            // available memory.
                            //
                            // Unfortunately, I currently can't think of an actual way to solve
                            // this cleanly for now.
                            let original_component_min_size = consumed_memblk.layout.size();

                            // Splits can be - in total - less than the original size. This picks
                            // how much of the original size is to be used. the +1 ensures that
                            // "use all of it" is valid
                            let to_use = u.choose_index(original_component_min_size + 1)?;

                            // Split point - we also use +1 on it because we want to test the case
                            // where the last layout is zero sized.
                            let split_point = u.choose_index(to_use + 1)?;

                            let first_half_size = split_point;
                            let second_half_size = to_use - split_point;

                            // Aligns can be anything
                            let first_half_align = arbitrary_alignment(u)?;
                            let second_half_align = arbitrary_alignment(u)?;

                            let produced_first_memblk = self.new_memory_block(
                                Layout::from_size_align(first_half_size, first_half_align)
                                    .expect("valid aligns produced and size isnt massive"),
                            );
                            let produced_second_memblk = self.new_memory_block(
                                Layout::from_size_align(second_half_size, second_half_align)
                                    .expect("valid aligns produced and size isnt massive or silly"),
                            );

                            Ok(AllocatorOperation::Split {
                                consumed_memblk,
                                fit_deviation,
                                produced_first_memblk,
                                produced_second_memblk,
                            })
                        }
                    }
                }
                SourceOrCreateMemblk::Create(new_block, zeroed) => {
                    Ok(AllocatorOperation::Allocate {
                        new_memblk: new_block,
                        zeroed: ZEROING_OPS && zeroed,
                    })
                }
            }
        }
    }

    /// Generate a new, arbitrary alignment that is always valid.
    fn arbitrary_alignment(u: &mut Unstructured) -> Result<usize, Error> {
        const MAX_ALIGN_EXPONENT: usize = 18;
        let align_exponent = u.int_in_range(0..=MAX_ALIGN_EXPONENT)?;
        Ok(1 << align_exponent)
    }

    /// Actual allocator operation.
    #[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
    pub(crate) enum AllocatorOperation {
        Allocate {
            new_memblk: MemoryBlock,
            zeroed: bool,
        },
        Deallocate {
            consumed_memblk: MemoryBlock,
            /// Encodes how much to deviate from the initial layout used to create the consumed
            /// memory block. This is taken as *modulo* the difference between the requested and
            /// actual size of the real allocated block. Fitdev of 1 will result in no deviation.
            fit_deviation: NonZeroUsize,
        },
        Grow {
            consumed_memblk: MemoryBlock,
            /// Encodes how much to deviate from the initial layout used to create the consumed
            /// memory block. This is taken as *modulo* the difference between the requested and
            /// actual size of the real allocated block. Fitdev of 1 will result in no deviation.
            fit_deviation: NonZeroUsize,
            produced_memblk: MemoryBlock,
            zeroed: bool,
        },
        Shrink {
            consumed_memblk: MemoryBlock,
            /// Encodes how much to deviate from the initial layout used to create the consumed
            /// memory block. This is taken as *modulo* the difference between the requested and
            /// actual size of the real allocated block. Fitdev of 1 will result in no deviation.
            fit_deviation: NonZeroUsize,
            produced_memblk: MemoryBlock,
        },
        Split {
            consumed_memblk: MemoryBlock,
            /// Encodes how much to deviate from the initial layout used to create the consumed
            /// memory block. This is taken as *modulo* the difference between the requested and
            /// actual size of the real allocated block. Note that it must be nonzero - fitdev of 1
            /// means no deviation.
            fit_deviation: NonZeroUsize,
            produced_first_memblk: MemoryBlock,
            produced_second_memblk: MemoryBlock,
        },
    }

    /// Represents a sequence of arbitrary allocations.
    ///
    /// The `MAX_OPS` constant param is the maximum amount of allocator operations to generate -
    /// this does NOT include operations generated by completion of allocations.
    ///
    /// The `COMPLETE` constant parameter dictates whether or not memory allocation must actually
    /// occur - defaults to true
    ///
    /// The `MAXIMUM_FITNESS_DEVIATION` constant parameter limits how much deviation can occur to
    /// test handling operations that consume memory blocks, where the provided layout may not be
    /// exactly the same as the layout used to allocate the block. It is *inclusive*. If you want
    /// no deviation, pass zero, as it will get added to `1` to get the inner modulo .
    /// Essentially, it is the maximum "length" of extra memory that can be encoded as a fitting layout
    /// for an allocated block, plus one.
    ///  
    ///  This is critically important, because when calculating memory operations ahead of time, it
    ///  is necessary to meet size constraints using the actual passed layout of the input memory
    ///  block rather than the layout used to allocate it. This can't be known ahead of time if the
    ///  passed layout has encoded deviation from the initial layout added to it, and hence when
    ///  calculating parameters ahead of time it is necessary to *pessimistically estimate* the
    ///  size.
    ///
    ///  Without constraints on the fitness deviation, the pessimistic estimation can grow
    ///  arbitrarily large, because the difference between the requested layout and actual returned
    ///  memory block can't be known, and hence the maximum possible size of input layouts is
    ///  arbitrary large (because the initial deviation is arbitrarily large).
    ///
    ///  However, by limiting the maximum deviation, that provides an absolute upper bound on the
    ///  actual input layout as calculated from the initial layout used to allocate a block, and
    ///  hence bounds the pessimistic size calculation.
    ///
    ///  The default is 1KiB - that is, no more than 1KiB of deviation between initial allocation
    ///  request and actually passed layout for operations will be tested. If your block size is
    ///  <=1KiB for an allocator, then every fitting memory layout can be tested, though if it's
    ///  more then only the first 1KiB-1byte of extra returned memory can be encoded in the layout passed
    ///  to functions that consume data.
    ///
    /// The `ZEROING_OPS` constant dictates whether or not to generate zeroing operations. By
    /// default these write to any related pointer, and this is not so desirable sometimes when
    /// testing. Of course, if your allocator does zeroing ops internally we can't help you.
    ///
    /// For an example of how to use this in fuzz testing, take a look at
    /// `fuzz/fuzz_targets/extended-allocator-arbitrary-event-validity.rs` in this repository.
    #[derive(Clone, PartialEq, Eq, Debug)]
    pub struct AllocatorEventSequence<
        const MAX_OPS: usize,
        const COMPLETE: bool = true,
        const MAXIMUM_FITNESS_DEVIATION: usize = 1024,
        const ZEROING_OPS: bool = true,
    > {
        allocator_operations: Vec<AllocatorOperation>,
    }

    impl<
            'a,
            const MAX_OPS: usize,
            const COMPLETE: bool,
            const MAXIMUM_FITNESS_DEVIATION: usize,
            const ZEROING_OPS: bool,
        > Arbitrary<'a>
        for AllocatorEventSequence<MAX_OPS, COMPLETE, MAXIMUM_FITNESS_DEVIATION, ZEROING_OPS>
    {
        fn arbitrary(u: &mut Unstructured<'a>) -> arbitrary::Result<Self> {
            let mut new_bulk_memory_blocks = AvailableMemoryBlocks::default();
            // Good heuristics here I think. Splitting cannot become exponential because each split
            // operation can only divide *one* memory block in two and hence each split is
            // equivalent to 2 allocations but 1 deallocation, and hence still a net single
            // allocation.
            let mut allocator_operations =
                Vec::with_capacity(if COMPLETE { MAX_OPS * 2 } else { MAX_OPS });
            u.arbitrary_loop(None, Some(MAX_OPS as u32), |u| {
                // Keep adding new operations.
                let op = new_bulk_memory_blocks
                    .new_operation::<MAXIMUM_FITNESS_DEVIATION, ZEROING_OPS>(u)?;
                allocator_operations.push(op);

                Ok(core::ops::ControlFlow::Continue(()))
            })?;

            // Complete any outstanding allocations
            //
            // Don't bother getting fit deviations - just set it to 1 (which means in actuality no
            // deviation).
            if COMPLETE {
                for (_id, still_allocated_memory_block) in new_bulk_memory_blocks.0 {
                    allocator_operations.push(AllocatorOperation::Deallocate {
                        consumed_memblk: still_allocated_memory_block,
                        fit_deviation: 1.try_into().expect("1 != 0"),
                    })
                }
            }

            Ok(Self {
                allocator_operations,
            })
        }
    }

    /// Validate that a pointer fits the layout, panicking if not, using asserts.
    pub(crate) fn valid_for_layout(layout: Layout, real_memory_block: NonNull<[u8]>) {
        // test size
        let actual_size = real_memory_block.len() * size_of::<u8>();
        assert!(
            actual_size >= layout.size(),
            "Allocated block (size {actual_size}) too small for layout {}",
            layout.size()
        );

        // would be better if we could use https://doc.rust-lang.org/core/ptr/struct.NonNull.html#method.as_non_null_ptr
        // but its nightly :'(
        let raw_byte_ptr = real_memory_block.as_ptr().cast::<u8>();

        // test alignment
        assert!(
            raw_byte_ptr.addr() % layout.align() == 0,
            "Allocated block address {raw_byte_ptr:?} not aligned to layout {}",
            layout.align()
        );
    }

    /// Like [`valid_for_layout`] but produces a closure for the purposes of easy use in
    /// [`Result::map`] and similar, that returns in-place and panics on invalid.
    pub(crate) fn validator_for_layout(layout: Layout) -> impl Fn(NonNull<[u8]>) -> NonNull<[u8]> {
        move |v| {
            valid_for_layout(layout, v);
            v
        }
    }

    /// Take a layout used for requests, the maximum deviation as randomly selected, and the actual
    /// memory block, and return the (ptr, layout) pair to pass through, where the layout is
    /// appropriately deviated for the original block length while still fitting the layout.
    ///
    /// The fit deviation modulo should be applied to the length. If it is one, no deviation
    /// occurs, if it is more then some amount occurs, but either way it:
    /// * Provides a maximum
    /// * Provides a source of randomness at the same time.
    pub(crate) fn deviate_layout(
        fit_deviation_mod: NonZeroUsize,
        initial_block_layout: Layout,
        actual_allocated_block: NonNull<[u8]>,
    ) -> (Layout, NonNull<u8>) {
        let fitting_room = actual_allocated_block.len() - initial_block_layout.size();
        let size_increase = fitting_room % fit_deviation_mod.get();
        let new_layout = Layout::from_size_align(
            initial_block_layout.size() + size_increase,
            initial_block_layout.align(),
        )
        .expect("size increase is fairly limited");
        (
            new_layout,
            actual_allocated_block
                .as_ptr()
                .cast::<u8>()
                .trans(NonNull::new)
                .expect("Came from nullptr"),
        )
    }

    impl<const MAX_OPS: usize, const COMPLETE: bool, const MAXIMUM_FITNESS_DEVIATION: usize, const ZEROING_OPS: bool>
        AllocatorEventSequence<MAX_OPS, COMPLETE, MAXIMUM_FITNESS_DEVIATION, ZEROING_OPS>
    {
        /// Do a basic runthrough of the event sequences to test for things like segfaults or
        /// too-short allocations.
        ///
        /// Note that AllocError(s) do NOT count for failure.
        ///
        /// Also note that this NEVER attempts to read or write from returned memory blocks. In
        /// particular, it is safe to use dummy allocators that just spit out dangling pointers if
        /// so desired, which is behaviour used to test the testing framework by using the debug
        /// assertions present in default impls to ensure validity.
        pub fn basic_extended_allocator_run(&self, allocator: impl ExtendedAllocator) {
            // Represents a real allocation - the layout used to request it, as well as the actual
            // allocation memory chunk.
            //
            // Note that if there was an allocation error, this is recorded
            type RealAllocation = (Layout, Result<NonNull<[u8]>, AllocError>);
            let mut allocations: HashMap<ID, RealAllocation> = Default::default();

            /// This performs an operation that is consuming on the given memory block - but *only*
            /// removes it if it did not have allocation failure, returning otherwise (doing
            /// nothing). Operation is a function that takes the layout initially used to allocate
            /// the memory block, and the actual memory block provided. Also provides access to the
            /// allocation map.
            ///
            /// Also panics if the ID isn't in the list of current allocations.
            fn consuming_operation_on(
                current_allocations: &mut HashMap<ID, RealAllocation>,
                id: ID,
                op: impl FnOnce(Layout, NonNull<[u8]>, &mut HashMap<ID, RealAllocation>),
            ) {
                // The inner allocation failed.
                if current_allocations
                    .get(&id)
                    .unwrap_or_else(|| panic!("Allocation with id {id} does not exist"))
                    .1
                    .is_err()
                {
                    return;
                };
                if let (_id, (initial_layout, Ok(actual_allocated_byte_slice))) =
                    current_allocations
                        .remove_entry(&id)
                        .expect("Just checked for validity")
                {
                    op(
                        initial_layout,
                        actual_allocated_byte_slice,
                        current_allocations,
                    )
                } else {
                    unreachable!()
                }
            }

            /// Like [`consuming_operation_on`], except it cleans up the consumed memory block if
            /// the inner operation fails for one reason or another, using allocator.deallocate.
            ///
            /// This is for instance if you run a grow or shrink operation and need to store it in
            /// the hashmap. If the allocation fails, return Err(AllocError) and this will clean up
            /// the consumed block under the *assumption* that the inner block did not consume it
            /// if it failed.
            ///
            /// # Safety
            /// The inner operation MUST NOT consume the block if it returns Err.
            unsafe fn destructor_insurance_consuming_operation_on(
                current_allocations: &mut HashMap<ID, RealAllocation>,
                id: ID,
                op: impl FnOnce(
                    Layout,
                    NonNull<[u8]>,
                    &mut HashMap<ID, RealAllocation>,
                ) -> Result<(), AllocError>,
                allocator: impl ExtendedAllocator,
            ) {
                consuming_operation_on(
                    current_allocations,
                    id,
                    |init_layout, actual_memory, allocation_collection| {
                        if op(init_layout, actual_memory, allocation_collection).is_err() {
                            // Clean up the block
                            // SAFETY: guarunteed by safety constraints of the outer function
                            unsafe {
                                allocator.deallocate(
                                    actual_memory
                                        .as_ptr()
                                        .cast::<u8>()
                                        .trans(NonNull::new)
                                        .expect("Just got out of nonnull"),
                                    init_layout,
                                )
                            }
                        }
                    },
                )
            }

            /// Panicky-insert the given allocation result with the given memory block id/layout.
            ///
            /// Also returns Result<(), AllocError> to aide in the destructing API.
            fn insert_allocation(
                current_allocations: &mut HashMap<ID, RealAllocation>,
                memory_block_data: MemoryBlock,
                actual_allocated_block: Result<NonNull<[u8]>, AllocError>,
            ) -> Result<(), AllocError> {
                let existing = current_allocations.insert(
                    memory_block_data.id,
                    (memory_block_data.layout, actual_allocated_block),
                );
                if existing.is_some() {
                    panic!("Duplicate allocation ID {}", memory_block_data.id)
                };
                match actual_allocated_block {
                    Ok(_) => Ok(()),
                    Err(e) => Err(e),
                }
            }

            for event in self.allocator_operations.iter() {
                match event {
                    AllocatorOperation::Allocate { new_memblk, zeroed } => {
                        let allocation_result = if *zeroed {
                            allocator.allocate_zeroed(new_memblk.layout)
                        } else {
                            allocator.allocate(new_memblk.layout)
                        }
                        .map(validator_for_layout(new_memblk.layout));

                        allocations
                            .insert(new_memblk.id, (new_memblk.layout, allocation_result))
                            .trans(|v| assert!(v.is_none(), "duplicate allocation IDs"));
                    }
                    AllocatorOperation::Deallocate {
                        consumed_memblk,
                        fit_deviation,
                    } => {
                        consuming_operation_on(
                            &mut allocations,
                            consumed_memblk.id,
                            |initial_layout, actual_allocated_block, _| {
                                // fuzz up the layout
                                let (actual_layout, extracted_ptr) = deviate_layout(
                                    *fit_deviation,
                                    initial_layout,
                                    actual_allocated_block,
                                );
                                // saftey: events are constructed to only use available memory blocks.
                                unsafe {
                                    allocator.deallocate(extracted_ptr, actual_layout);
                                }
                            },
                        )
                    }
                    AllocatorOperation::Grow {
                        consumed_memblk,
                        fit_deviation,
                        produced_memblk,
                        zeroed,
                    } =>
                    /* SAFETY: grow is the only allocator op, and it is atomic */
                    unsafe {
                        destructor_insurance_consuming_operation_on(
                            &mut allocations,
                            consumed_memblk.id,
                            |initial_layout,
                             actual_to_consume_allocated_block,
                             allocs|
                             -> Result<(), AllocError> {
                                // Fuzz up layout
                                let (initial_layout, actual_to_consume_allocated_block) =
                                    deviate_layout(
                                        *fit_deviation,
                                        initial_layout,
                                        actual_to_consume_allocated_block,
                                    );

                                // SAFETY:
                                // The event auto-validates the layout and similar stuff.
                                // In the case that failure occurs, we document that failure with
                                // the ID of the new memory blocks.
                                let maybe_new_allocated_block = if *zeroed {
                                    allocator.grow_zeroed(
                                        actual_to_consume_allocated_block,
                                        initial_layout,
                                        produced_memblk.layout,
                                    )
                                } else {
                                    allocator.grow(
                                        actual_to_consume_allocated_block,
                                        initial_layout,
                                        produced_memblk.layout,
                                    )
                                }
                                .map(validator_for_layout(produced_memblk.layout));

                                insert_allocation(
                                    allocs,
                                    *produced_memblk,
                                    maybe_new_allocated_block,
                                )
                            },
                            &allocator,
                        )
                    },
                    AllocatorOperation::Shrink {
                        consumed_memblk,
                        fit_deviation,
                        produced_memblk,
                    } =>
                    /* SAFETY: inner function only uses one atomic alloc op, namely the shrinking*/
                    unsafe {
                        destructor_insurance_consuming_operation_on(
                            &mut allocations,
                            consumed_memblk.id,
                            |initial_layout,
                             actual_to_consume_allocated_block,
                             allocs|
                             -> Result<(), AllocError> {
                                // Fuzz up layout
                                let (initial_layout, actual_to_consume_allocated_block) =
                                    deviate_layout(
                                        *fit_deviation,
                                        initial_layout,
                                        actual_to_consume_allocated_block,
                                    );

                                // SAFETY: generated event parameters *should* be valid.
                                let maybe_successful_alloc = allocator
                                    .shrink(
                                        actual_to_consume_allocated_block,
                                        initial_layout,
                                        produced_memblk.layout,
                                    )
                                    .map(validator_for_layout(produced_memblk.layout));

                                insert_allocation(allocs, *produced_memblk, maybe_successful_alloc)
                            },
                            &allocator,
                        )
                    },
                    AllocatorOperation::Split {
                        consumed_memblk,
                        fit_deviation,
                        produced_first_memblk,
                        produced_second_memblk,
                    } =>
                    /* SAFETY: split operation is sole, atomic allocator operation */
                    unsafe {
                        destructor_insurance_consuming_operation_on(
                            &mut allocations,
                            consumed_memblk.id,
                            |initial_layout,
                             actual_to_consume_allocated_block,
                             allocs|
                             -> Result<(), AllocError> {
                                let (initial_layout, actual_to_consume_allocated_block) =
                                    deviate_layout(
                                        *fit_deviation,
                                        initial_layout,
                                        actual_to_consume_allocated_block,
                                    );
                                // SAFETY: Generated parameters *should* be valid.
                                let maybe_successful_alloc_pair = allocator
                                    .split(
                                        actual_to_consume_allocated_block,
                                        initial_layout,
                                        produced_first_memblk.layout,
                                        produced_second_memblk.layout,
                                    )
                                    .map(|(fst, scd)| {
                                        // Validate each and rereturn tuple
                                        valid_for_layout(produced_first_memblk.layout, fst);
                                        valid_for_layout(produced_second_memblk.layout, scd);
                                        (fst, scd)
                                    });

                                // We then SPLIT the result into a pair of results
                                //
                                // These have identical Err/Ok statuses - as such, we only need to
                                // return the result of the latter insert shorthand fn and can
                                // forget the first result.
                                let (fst_maybe_block, scd_maybe_block) =
                                    match maybe_successful_alloc_pair {
                                        Ok((fst, scd)) => (Ok(fst), Ok(scd)),
                                        Err(e) => (Err(e), Err(e)),
                                    };

                                let _ = insert_allocation(
                                    allocs,
                                    *produced_first_memblk,
                                    fst_maybe_block,
                                );
                                insert_allocation(allocs, *produced_second_memblk, scd_maybe_block)
                            },
                            &allocator,
                        )
                    },
                }
            }
        }
    }
}

// core-alloc
// Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
