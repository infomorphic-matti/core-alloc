# core-alloc
Provides specialised allocation capabilities, including instrumentation of allocators and restriction of memory usage (both with soft usage heuristics, and with precisely controlled heuristics with the latter requiring allocator cooperation to implement).

This library works even in environments without the standard library's `liballoc` available, and can handle ultra-constrained environments.

## Allocator API
This library uses an extended version of the `allocator_api` nightly feature as the basis of it's allocators.

This extended version allows for splitting an allocation using more efficient methods than allocating something new and copying data, if the allocator provides the facility. In embedded devices or constrained environments where simpler allocators are common, this is often possible.  

## Security and Hardening Capabilities
This library provides two levels of memory usage tracking, instrumentation, and restriction. *Soft* restriction requires no cooperation from an underlying allocator - however, because of the requirements of the rust allocator API, it can only provide rougher estimates of memory usage for the purposes of tracking and management.

Hard restriction/instrumentation - designed for extremely security concious users or those in highly restricted environments - tracks a complete and exact evaluation of how much memory is dynamically used, including for dynamic allocator bookkeeping, which would otherwise provide a potential route for attack (in particular, forcing large numbers of tiny allocations could be used to cause memory overuse for bookkeeping). 

Hard allocation tracking requires cooperation from the allocator, because the allocator API is by default unaware of internal state usage and does not provide information on things like allocator block sizes and padding amounts, which is required to instrument an allocator properly.

## Fuzzing
This crate provides utilities for fuzzing your allocators for valid behaviour.

It includes the `arbitrary` crate as an optional feature.
